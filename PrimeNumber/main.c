
#include <stdlib.h>
#include <stdio.h>
#include "../CDataStructure/LinkedList.h"
#include "../customdef.h"

int primeList(LinkedList* list, int max);

int main(int argc, char** argv)
{
	int max;

	if(argc >= 2)
	{
		max = atoi(argv[1]);
	}
	else
	{
		printf("Specify the range to find prime numbers 2~");
		fflush(stdin);
		if(1 != scanf("%d", &max))
		{
			fprintf(stderr, "Not integer.");
			exit(1);
		}
	}

	LinkedList prime; LListInit(&prime);
	int noOfPrime = primeList(&prime, max);
	if(0 == noOfPrime)
	{
		printf("Enter a number greater than or equal to 2.");
		return 0;
	}

	ListSet(&prime);
	while(ListNext(&prime))
	{
		printf("%d ", *(int*)ListRead(&prime));
	}
	printf("\n%d prime numbers found.", noOfPrime);

	ListFree(&prime);

	return 0;
}
