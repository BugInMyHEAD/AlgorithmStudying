#include "../CDataStructure/LinkedList.h"
#include "../customdef.h"


DECLARE_LLIST(int, int)


/**
 * 소수들의 리스트를 생성
 *
 * @param initialized 초기화된 LList_int에 소수들을 저장
 * @param maxin 조사할 수의 범위
 * @return 소수의 개수
 */
int primeList(LList_int* initialized, int maxin)
{
	const int easyPrime[] = {
		2, 3, 5, 7, 11, 13, 17, 19,
		23, 29, 31, 37, 41, 43, 47,
		53, 59, 61, 67, 71, 73, 79, };

	int noOfPrime;
	LList_int_Iter iter; LList_int_iterHead(initialized, &iter);

	for (noOfPrime = 0;
			noOfPrime < ARRLEN(easyPrime) && easyPrime[noOfPrime] <= maxin;
			++noOfPrime)
	{
		LList_int_addPrev(&iter, easyPrime[noOfPrime]);
	}

	for(int i1 = ARRLAST(easyPrime) + 2; i1 <= maxin; i1 += 2)
	{
		for(LList_int_goHead(&iter); ; )
		{
			LList_int_goNext(&iter);

			int prime = LList_int_getData(&iter);
			int quotient = i1 / prime;
			if(quotient < prime)
			{
				LList_int_goHead(&iter);
				LList_int_addPrev(&iter, i1);
				++noOfPrime;
				break;
			}
			else if(quotient * prime == i1)
			{
				break;
			}
		}
	}

	LList_int_iterUnref(&iter);

	return noOfPrime;
}
