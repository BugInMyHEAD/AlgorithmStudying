/**
 * Binary Search Tree
 *
 * BinarySearchTree.h에서 선언하는 함수는 BST라는 접두어가 붙음
 *
 * 구현 및 private 함수, 기타 세부사항에 대한 문서는
 *  BinarySearchTree.c에 있음
 */


#include "BinaryTree.h"


typedef struct BstNode_postfix BstNode_postfix;
typedef struct Bst_postfix Bst_postfix;

struct BstNode_postfix
{
	BinaryTreeNode_postfix binaryTreeNode;
	size_t numDescendant;
};

struct Bst_postfix
{
	BstNode_postfix* root;
	int(* comparer)(BinaryTreeData, BinaryTreeData);
};


/**
 * BinarySearchTree 초기화
 */
void Bst_postfix_construct(
	Bst_postfix* bst,
	int(* comparer)(const void*, const void*));

/**
 * BinarySearchTree 소멸
 */
void Bst_postfix_destruct(Bst_postfix* bst);

/**
*
*
* @return HeapData 두 개의 우선순위를 평가하는 함수 포인터
*         해당 함수의 반환값이 음수이면 첫 번째 인자의 우선순위가 높다고 가정
*         해당 함수의 반환값이 0이면 두 인자의 우선순위가 동일하다고 가정
*         해당 함수의 반환값이 양수이면 두 번째 인자의 우선순위가 높다고 가정
*/
int(* Bst_postfix_getComparer(const Bst_postfix* bst))(const void*, const void*);

/**
 *
 *
 * @return BinarySearchTree를 구성하는 이진 트리의 루트 노드 주소
 */
/*const BinaryTreeNode* BST_getRoot(const BinarySearchTree* bst);*/

/** 
 * BinarySearchTree에 BinaryTreeData 삽입
 *
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
int Bst_postfix_insertDup(Bst_postfix* bst, BinaryTreeData data);

/** 
 * BinarySearchTree에서 특정 BinaryTreeData를 삭제
 *
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
BinaryTreeNode_postfix* Bst_postfix_delete(Bst_postfix* bst, BinaryTreeData target);

/** 
 * BinarySearchTree 순회
 *
 * @param binaryTree_trav BinaryTree.h에 선언된 순회 함수 포인터
 * @param action 순회 시 수행될 행동
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
int Bst_postfix_traverse(
	const Bst_postfix* bst,
	int(* binaryTree_trav)(const BinaryTreeNode_postfix*, void(*)(BinaryTreeData)),
	void(* action)(BinaryTreeData));
