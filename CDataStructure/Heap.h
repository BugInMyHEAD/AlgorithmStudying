/**
 * Heap Library
 *
 * 구현 및 private 함수, 기타 세부사항에 대한 문서는
 * Heap.c에 있음
 */


#pragma once


#include "LinkedList.h"


typedef enum EHeapError
{
	/**
	 * It is guaranteed that HEAP_SUCCESS is 0
	 */
	HEAP_SUCCESS = 0,
	
	/**
	 * Failure because of lack of free memory
	 */
	HEAP_NOMEM = INT_MIN >> 2,
} EHeapError;


typedef int type;

typedef struct Heap_postfix Heap_postfix;


#define DEFINE_HEAP(postfix, type) \


#define DECLARE_HEAP(postfix, type) \
DECLARE_LLIST(postfix##_p, type*)
DECLARE_LLIST(postfix_p, type*)

/**
 * Heap 핵심 구조체
 * 사용 전에 Heap_construct로 초기화하여야 함
 *
 * @see Heap_construct
 */
struct Heap_postfix
{
	/**
	 * 
	 */
	LList_postfix_p llist;

	/**
	 *
	 */
	LList_postfix_p_Iter bufOfLastElm;

	/**
	 * HeapData 두 개의 우선순위를 평가하는 함수 포인터
	 * 해당 함수의 반환값이 음수이면 첫 번째 인자의 우선순위가 높다고 가정
	 * 해당 함수의 반환값이 0이면 두 인자의 우선순위가 동일하다고 가정
	 * 해당 함수의 반환값이 양수이면 두 번째 인자의 우선순위가 높다고 가정
	 */
	int(* comparer)(const void*, const void*);

	/**
	 * Maximum buffer size among allocated buffers.
	 *
	 * This can be calculated from size of LList_postfix_p_Iter of llist but
	 * by using function double pow(double, double), it would be slow.
	 */
	size_t maxBound;

	/**
	 * Buffer size being used by the last element.
	 *
	 * This can be calculated from index of LList_postfix_p_Iter of llist but
	 * by using function double pow(double, double), it would be slow.
	 */
	size_t bound;

	/**
	 * For internal indexing for the buffer of the last element.
	 */
	size_t index;
};


/**
 * Heap 초기화
 *
 * @param capacity 최대 요소 개수. 동적 할당 크기에 영향을 미침.
 * @param comparer HeapData 두 개의 우선순위를 평가하는 함수 포인터
 *        해당 함수의 반환값이 음수이면 첫 번째 인자의 우선순위가 높다고 가정
 *        해당 함수의 반환값이 0이면 두 인자의 우선순위가 동일하다고 가정
 *        해당 함수의 반환값이 양수이면 두 번째 인자의 우선순위가 높다고 가정
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
EHeapError Heap_postfix_construct(
	Heap_postfix* heap, int(* comparer)(const void*, const void*));

/**
 * Heap 소멸
 */
void Heap_postfix_destruct(Heap_postfix* heap);

/**
 *
 *
 * @return Heap에 들어있는 요소의 수
 */
size_t Heap_postfix_getSize(const Heap_postfix* heap);

/**
 *
 *
 * @return HeapData 두 개의 우선순위를 평가하는 함수 포인터
 *         해당 함수의 반환값이 음수이면 첫 번째 인자의 우선순위가 높다고 가정
 *         해당 함수의 반환값이 0이면 두 인자의 우선순위가 동일하다고 가정
 *         해당 함수의 반환값이 양수이면 두 번째 인자의 우선순위가 높다고 가정
 */
int(* Heap_postfix_getComparer(const Heap_postfix* heap))(const void*, const void*);

/**
 *
 *
 * @return Heap에 들어있는 요소의 수가 0이면 1, 그렇지 않으면 0
 */
int Heap_postfix_isEmpty(const Heap_postfix* heap);

/**
 * Heap에서 가장 우선순위가 높은 요소를 data에 복사
 *
 * @param data 가장 우선순위가 높은 요소를 복사받을 포인터
 * @return 성공 시 0, 실패(Heap에 들어있는 요소의 수가 0) 시
 *         음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
int Heap_postfix_peek(const Heap_postfix* heap, type* data);

/**
 * Heap에 HeapData를 삽입
 *
 * @param data Heap에 삽입할 HeapData
 * @return 성공 시 0, 실패(Heap에 들어있는 요소의 수가 최대) 시
 *         음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
int Heap_postfix_insert(Heap_postfix* heap, type data);

/**
 * Heap에서 가장 우선순위가 높은 요소를 data에 복사하고 Heap에서 제거
 *
 * @param data 가장 우선순위가 높은 요소를 복사받을 포인터
 * @return 실패(Heap에 들어있는 요소의 수가 0) 시 0,
 *         성공 시 0이 아닌 수
 */
int Heap_postfix_delete(Heap_postfix* heap, type* data);
