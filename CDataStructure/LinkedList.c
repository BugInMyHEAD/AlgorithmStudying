/*
Copyright(c) 2018 BugInMyHEAD

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/


/**
 * 이터레이터 패턴이 적용된 doubly linked circular list
 * Dummy head node를 가지며, tail node는 따로 표시하지 않음
 *
 * LinkedList.h에서 정의하는 library는 LList라는 접두어가 붙음
 * Fwd는 forward, Rev는 reverse의 약어
 */


/* Public 함수에 대한 문서는 LinkedList.h에 있음 */


#include "../customdef.h"
#include "LinkedList.h"


/**
 * Chain two nodes
 */
static void LListChain(LListNode* prev, LListNode* next)
{
	prev->next = next;
	next->prev = prev;
}


/**
 * @return The list referenced by 'a' and by 'b',
 *         NULL if their referencing lists are different.
 */
static LinkedList* LListAgree(const LListIterator* a, const LListIterator* b)
{
	LinkedList* list = LListGetList(a);
	if (list != LListGetList(b))
	{
		return NULL;
	}

	return list;
}


/**
 * @return The number of all nodes in the list including the head node
 */
static size_t LListGetActualSize(const LinkedList* list)
{
	return LListGetSize(list) + 1;
}


static size_t LListGetRevIndex(size_t size, size_t index)
{
	return ( size - index ) % ( size + 1 ) - 1;
}


void LListInit(LinkedList* list)
{
	/* To form a doubly linked circular list */
	list->head.next = list->head.prev = &list->head;
	/* Note NULL doesn't guarantee that the data is of the head node
	 * Note the data of the head node is NOT guaranteed to be NULL */
	list->head.data = NULL;
	list->iterCount = 0;
	list->size = 0;
}


/**
 * 초기화된 LListNode 동적 할당
 *
 * @return 동적 할당 실패 시 NULL, 성공 시 할당된 주소의 포인터
 */
static LListNode* LListNodeMallocAndInit(LListNode* prev, LListNode* next, void* data)
{
	LListNode * result;
	if (MALLOC_ASSIGN(result, 1))
	{
		LListChain(result, next);
		LListChain(prev, result);
		result->data = data;
	}

	return result;
}


LinkedList* LListMallocAndInit(void)
{
	LinkedList * result;
	if (MALLOC_ASSIGN(result, 1))
	{
		LListInit(result);
	}

	return result;
}


size_t LListGetSize(const LinkedList* list)
{
	return list->size;
}


int LListGetIterCount(const LinkedList* list)
{
	return list->iterCount;
}


void LListIterDestruct(LListIterator* iter)
{
	LListGetList(iter)->iterCount--;
	iter->list = NULL;
	iter->node = NULL;
}


void LListIterHead(LinkedList* list, LListIterator* iter)
{
	iter->list = list;
	iter->index = 0;
	iter->node = &list->head;
	list->iterCount++;
}


ELListError LListIterCopy(LListIterator* dest, const LListIterator* src)
{
	LinkedList* list = LListAgree(dest, src);
	if (!list)
	{
		return LLIST_DISAGREE;
	}

	*dest = *src;
	list->iterCount++;

	return LLIST_SUCCESS;
}


LListIterator* LListIterMallocIterHead(LinkedList* list)
{
	LListIterator * result;
	if (MALLOC_ASSIGN(result, 1))
	{
		LListIterHead(list, result);
		list->iterCount++;
	}

	return result;
}


ELListError LListInvert(LListIterator* iter)
{
	LinkedList * list = LListGetList(iter);

	if (LListGetIterCount(list) != 1)
	{
		return LLIST_ANOTHER_ITER;
	}

	/**
	 * Head node부터 'prev' 방향으로(전치되었으므로)
	 * head node 직전 node까지
	 *
	 * iter.index의 변화는 신경쓰지 않는다
	 */
	LListIterator temp; LListIterHead(list, &temp);
	do
	{
		SWAP(temp.node->prev, temp.node->next);
	} while (LListGoNext(&temp));

	iter->index = LListGetIndexRev(iter);

	return LLIST_SUCCESS;
}


int LListIterFirst(LinkedList* list, LListIterator* iter)
{
	return LListIterFwd(list, iter, 0);
}


int LListIterLast(LinkedList* list, LListIterator* iter)
{
	return LListIterRev(list, iter, 0);
}


/**
 * LListIterFwd와 LListIterRev의 공통 부분
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * @param llistGoAbs LListGoAbsFwd 또는 LListGoAbsRev
 * @return 인덱스 범위(0(head node)에서 iter->list->size)를 벗어나면 0,
 *         그렇지 않으면 1
 */
static int LListIter(
	LinkedList* list,
	LListIterator* iter,
	size_t index,
	int(*llistGoAbs)(LListIterator*, size_t))
{
	LListIterator temp; LListIterHead(list, &temp);
	int result = llistGoAbs(&temp, index);
	if (result)
	{
		*iter = temp;
	}
	else
	{
		LListIterDestruct(&temp);
	}

	return result;
}


int LListIterFwd(LinkedList* list, LListIterator* iter, size_t index)
{
	return LListIter(list, iter, index, LListGoAbsFwd);
}


int LListIterRev(LinkedList* list, LListIterator* iter, size_t index)
{
	return LListIter(list, iter, index, LListGoAbsRev);
}


/**
 * 이터레이터가 head node를 가리키는지 확인
 *
 * @return iter가 head node를 가리키면 1, 그렇지 않으면 0
 */
int LListIsHead(const LListIterator* iter)
{
	return iter->node == &LListGetList(iter)->head;
}


/**
 * LListGoNext와 LListGoPrev의 공통 부분
 * 이터레이터가 인접하는 다음 혹은 이전 노드를 가리키게 함
 * 실패하지 않는 함수이며, 반환값은 head node인지 구별하기 위함
 *
 * @param offset  prev 방향 함수이면 -1, next 방향 함수이면 +1
 *                Undefined behavior if offset is neither -1 nor +1
 * @return 수행 후에 이터레이터가 가리키는 노드가 head node(index == 0)이면 0,
 *         그 외에는 1을 반환
 */
static int LListGoAdj(LListIterator* iter, LListNode* node, size_t offset)
{
	iter->node = node;
	size_t actualSize = LListGetActualSize(LListGetList(iter));
	int result = !LListIsHead(iter);
	iter->index = ( iter->index + 1 + offset + actualSize ) % actualSize - 1;

	return result;
}


int LListGoNext(LListIterator* iter)
{
	return LListGoAdj(iter, iter->node->next, +1);
}


int LListGoPrev(LListIterator* iter)
{
	return LListGoAdj(iter, iter->node->prev, -1);
}


/**
 * LListGoAbsFwd와 LListGoAbsRev,
 * LListGoRelFwd, LListGoRelRev의 공통 부분
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * @param rev  prev 방향 함수이면 1, next 방향 함수이면 0
 * @return 1
 *         0
 */
static int LListGoAbs(LListIterator* iter, size_t index, int rev)
{
	size_t size = LListGetSize(LListGetList(iter));
	/* 'Go'ing to the head node by this function is not allowed */
	if (index >= size)
	{
		return 0;
	}

	size_t pastIndex = LListGetIndex(iter);
	/* 노드 이동 경로 최적화
	 *
	 * iter->node로부터 출발하는 것이 빠른지,
	 * head node로부터 출발하는 것이 빠른지 평가
	 *
	 * absolute는 head node로부터의 거리,
	 * relative는 iter->node로부터의 거리임
	 * offset에 위 둘 중에 짧은 거리를 배정
	 *
	 * absolute, relative, rev에 의해 이동 방향 결정(llistGoAdj) */
	size_t absolute, relative;
	size_t offset;
	int(*llistGoAdj)(LListIterator*);
	if (rev)
	{
		relative = pastIndex - index;
		absolute = index - SIZE_MAX;
		if (absolute >= relative)
		{
			llistGoAdj = LListGoPrev;
			offset = relative;
		}
		else
		{
			LListGoHead(iter);
			llistGoAdj = LListGoNext;
			offset = absolute;
		}
	}
	else
	{
		relative = index - pastIndex;
		absolute = size - index;
		if (relative <= absolute)
		{
			llistGoAdj = LListGoNext;
			offset = relative;
		}
		else
		{
			LListGoHead(iter);
			llistGoAdj = LListGoPrev;
			offset = absolute;
		}
	}
	for (size_t i1 = 0; i1 < offset; ++i1)
	{
		llistGoAdj(iter);
	}

	return 1;
}


int LListGoAbsFwd(LListIterator* iter, size_t index)
{
	return LListGoAbs(iter, index, 0);
}


int LListGoAbsRev(LListIterator* iter, size_t index)
{
	return LListGoAbs(iter, index, 1);
}


int LListGoRelFwd(LListIterator* iter, size_t offset)
{
	return LListGoAbsFwd(iter, LListGetIndex(iter) + offset);
}


int LListGoRelRev(LListIterator* iter, size_t offset)
{
	return LListGoAbsFwd(iter, LListGetIndex(iter) - offset);
}


void LListGoHead(LListIterator* iter)
{
	iter->index = SIZE_MAX;
	iter->node = &LListGetList(iter)->head;
}


int LListGoFirst(LListIterator* iter)
{
	return LListGoAbsFwd(iter, 0);
}


int LListGoLast(LListIterator* iter)
{
	return LListGoAbsRev(iter, 0);
}


LListData LListGetData(const LListIterator* iter)
{
	return iter->node->data;
}


LinkedList* LListGetList(const LListIterator* iter)
{
	return iter->list;
}


size_t LListGetIndex(const LListIterator* iter)
{
	return iter->index;
}


size_t LListGetIndexRev(const LListIterator* iter)
{
	LinkedList * list = LListGetList(iter);
	
	return LListGetRevIndex(LListGetSize(list), LListGetIndex(iter));
}


/**
 * LListAddNext와 LListAddPrev의 공통 부분
 *
 * prev와 next 사이에 새 노드가 끼워넣어짐
 *
 * @return LLIST_WRITER_EXCEEDS
 *         LLIST_NOMEM
 *         LLIST_SUCCESS
 */
static ELListError LListAdd(
	LinkedList* list,
	LListData data,
	LListNode* prev,
	LListNode* next)
{
	if (LListGetIterCount(list) != 1)
	{
		return LLIST_ANOTHER_ITER;
	}

	LListNode * node = LListNodeMallocAndInit(prev, next, data);
	if (!node)
	{
		return LLIST_NOMEM;
	}

	list->size++;

	return LLIST_SUCCESS;
}


ELListError LListAddNext(const LListIterator* iter, LListData data)
{
	LinkedList * list = LListGetList(iter);

	return LListAdd(list, data, iter->node, iter->node->next);
}


ELListError LListAddLast(const LListIterator* iter, LListData data)
{
	LinkedList * list = LListGetList(iter);

	return LListAdd(list, data, list->head.prev, &list->head);
}


static ELListError LListAddPrevOrFirst(
	LListIterator* iter,
	LinkedList* list,
	LListData data,
	LListNode* prev,
	LListNode* next)
{
	ELListError result = LListAdd(list, data, prev, next);
	if (result == LLIST_SUCCESS && !LListIsHead(iter))
	{
		iter->index++;
	}

	return result;
}


ELListError LListAddPrev(LListIterator* iter, LListData data)
{
	LinkedList * list = LListGetList(iter);

	return LListAddPrevOrFirst(iter, list, data, iter->node->prev, iter->node);
}


ELListError LListAddFirst(LListIterator* iter, LListData data)
{
	LinkedList * list = LListGetList(iter);

	return LListAddPrevOrFirst(iter, list, data, &list->head, list->head.next);
}


/**
 * LListRmGoNext와 LListRmGoPrev의 공통 부분
 *
 * @param llistGoAdj  LListGoNext 또는 LListGoPrev
 */
static ELListError LListRmGo(
	LListIterator* iter,
	LListData* buffer,
	int(*llistGoAdj)(LListIterator*))
{
	LinkedList * list = LListGetList(iter);

	if (LListGetIterCount(list) != 1)
	{
		return LLIST_ANOTHER_ITER;
	}

	LListChain(iter->node->prev, iter->node->next);

	LListNode * removed = iter->node;
	if (buffer)
	{
		*buffer = removed->data;
	}
	llistGoAdj(iter);

	free(removed);

	list->size--;

	return LLIST_SUCCESS;
}


ELListError LListRmGoNext(LListIterator* iter, LListData buffer)
{
	iter->index--;

	return LListRmGo(iter, buffer, LListGoNext);
}


ELListError LListRmGoPrev(LListIterator* iter, LListData buffer)
{
	return LListRmGo(iter, buffer, LListGoPrev);
}


ELListError LListMove(
	LListIterator* paster,
	LListIterator* cutBeginInc,
	LListIterator* cutEndExc)
{
	LinkedList * pasted =  LListGetList(paster);
	LinkedList * cut = LListAgree(cutBeginInc, cutEndExc);
	if (!cut)
	{
		return LLIST_DISAGREE;
	}

	if (LListGetIterCount(pasted) != 1 || LListGetIterCount(cut) != 2)
	{
		return LLIST_ANOTHER_ITER;
	}
	
	size_t beginIdx = cutBeginInc->index;
	size_t endIdx = cutEndExc->index;
	size_t qty;
	if (beginIdx != endIdx)
	{
		if (beginIdx > endIdx)
		{
			LListChain(cut->head.prev, cut->head.next);
			LListChain(cutBeginInc->node->prev, &cut->head);
			LListChain(&cut->head, cutEndExc->node);
			qty = beginIdx - endIdx - 1;
			cutEndExc->index = 0;
		}
		else
		{
			LListChain(cutBeginInc->node->prev, cutEndExc->node);
			qty = endIdx - beginIdx;
			cutEndExc->index -= qty;
		}
		LListChain(paster->node->prev, cutBeginInc->node);
		LListChain(paster->node, cutEndExc->node->prev);
		cut->size -= qty;
		pasted->size += qty;
		if (!LListIsHead(paster))
		{
			paster->index += qty;
		}
		cutBeginInc = cutEndExc;
	}

	return LLIST_SUCCESS;
}
