/**
 * Heap Library
 *
 * 배열을 이용한 힙 구현
 * 배열 인덱스 0부터 요소를 채움
 *
 * Public 함수에 대한 문서는 Heap.h에 있음
 */


#include <stdlib.h>

#include "../customdef.h"

#include "Heap.h"


 /**
  * 힙의 각 노드의 최대 서브트리의 수
  */
#define HEAP_NUM_CHILD 2


static size_t Heap_postfix_obtainNextBoundSize(size_t bound)
{
	return !!bound * HEAP_NUM_CHILD * bound + !bound;
}


static size_t Heap_postfix_obtainPrevBoundSize(size_t bound)
{
	return bound / HEAP_NUM_CHILD;
}


static size_t Heap_postfix_obtainStartSize(size_t bound)
{
	return ( bound - 1 + !bound ) / ( HEAP_NUM_CHILD - 1 );
}


/**
* 부모의 인덱스를 계산
*
* @param childIdx 자식으로 가정하는 요소의 인덱스. childIdx < SIZE_MAX
* @return 부모의 인덱스
*/
static size_t Heap_postfix_obtainParentIdx(size_t childIdx)
{ 
	return childIdx / HEAP_NUM_CHILD; 
}


/**
* 가장 왼쪽 자식의 인덱스를 계산
*
* @param parentIdx 부모로 가정하는 요소의 인덱스
* @return 가장 왼쪽 자식의 인덱스. 오버플로우 가능성 있음.
*/
static size_t Heap_postfix_obtainLeftmostChildIdx(size_t parentIdx)
{ 
	return parentIdx * HEAP_NUM_CHILD;
}


EHeapError Heap_postfix_construct(
	Heap_postfix* heap, int(* comparer)(const void*, const void*))
{
	LList_postfix_p_init(&heap->llist);
	LList_postfix_p_iterHead(&heap->llist, &heap->bufOfLastElm);

	heap->bound = heap->maxBound = 0;
	heap->index = SIZE_MAX;
	heap->comparer = comparer;

	return HEAP_SUCCESS;
}


void Heap_postfix_destruct(Heap_postfix* heap)
{
	if (LList_postfix_p_goLast(&heap->bufOfLastElm))
	{
		while (!LList_postfix_p_isHead(&heap->bufOfLastElm))
		{
			type* freed;
			LList_postfix_p_removeGoPrev(&heap->bufOfLastElm, &freed);
			FREE_ASSIGN(freed);
		}
	}

	LList_postfix_p_iterUnref(&heap->bufOfLastElm);
}


size_t Heap_postfix_getSize(const Heap_postfix* heap)
{
	return Heap_postfix_obtainStartSize(heap->bound) + heap->index + 1;
}


int(* Heap_postfix_getComparer(const Heap_postfix* heap))(const void*, const void*)
{
	return heap->comparer;
}


int Heap_postfix_isEmpty(const Heap_postfix* heap)
{
	return !Heap_postfix_getSize(heap);
}


int Heap_postfix_peek(const Heap_postfix* heap, type* buffer)
{
	if (Heap_postfix_isEmpty(heap))
	{
		return 0;
	}

	type* firstBuffer;
	LList_postfix_p_getDataAt(&heap->bufOfLastElm, 0, &firstBuffer);
	*buffer = firstBuffer[0];

	return 1;
}


EHeapError Heap_postfix_insert(Heap_postfix* heap, type data)
{
	size_t nextIndex = heap->index + 1;
	if (nextIndex == heap->bound)
	{
		size_t nextBoundSize = Heap_postfix_obtainNextBoundSize(heap->bound);

		if (nextIndex == heap->maxBound)
		{
			type* buffer;
			if (!MALLOC_ASSIGN(buffer, nextBoundSize))
			{
				return HEAP_NOMEM;
			}

			if (LList_postfix_p_addNext(&heap->bufOfLastElm, buffer) ==
				LLIST_NOMEM)
			{
				FREE_ASSIGN(buffer);
				return HEAP_NOMEM;
			}
			heap->maxBound = nextBoundSize;
		}

		nextIndex = 0;
		heap->bound = nextBoundSize;
		LList_postfix_p_goNext(&heap->bufOfLastElm);
	}
	heap->index = nextIndex;

	int(* comparer)(const void*, const void*) = Heap_postfix_getComparer(heap);

	LList_postfix_p_Iter pIter;
	LList_postfix_p_copyIter(&pIter, &heap->bufOfLastElm);

	size_t parentIdx, childIdx;
	type* pIterData, * cIterData;
	childIdx = nextIndex;
	cIterData = LList_postfix_p_getData(&pIter);
	while (1)
	{
		LList_postfix_p_goPrev(&pIter);
		pIterData = LList_postfix_p_getData(&pIter);
		parentIdx = Heap_postfix_obtainParentIdx(childIdx);

		/* 루트가 아니고 부모의 우선순위가 더 높지 않은 동안 */
		if (LList_postfix_p_isHead(&pIter) ||
			comparer(&pIterData[parentIdx], &data) < 0)
		{
			break;
		}

		cIterData[childIdx] = pIterData[parentIdx];
		cIterData = pIterData;
		childIdx = parentIdx;
	}
	cIterData[childIdx] = data;

	LList_postfix_p_iterUnref(&pIter);

	return HEAP_SUCCESS;
}


int Heap_postfix_delete(Heap_postfix* heap, type* buffer)
{
	if (!Heap_postfix_peek(heap, buffer))
	{
		return 0;
	}

	int(* comparer)(const void*, const void*) = Heap_postfix_getComparer(heap);

	LList_postfix_p_Iter cIter; LList_postfix_p_iterHead(&heap->llist, &cIter);
	LList_postfix_p_goNext(&cIter);

	type* bufOfLastElmData = LList_postfix_p_getData(&heap->bufOfLastElm);
	type lastData = bufOfLastElmData[heap->index];
	size_t parentIdx, childIdx;
	type* pIterData, * cIterData;
	parentIdx = 0;
	pIterData = LList_postfix_p_getData(&cIter);
	while (pIterData != bufOfLastElmData)
	{
		LList_postfix_p_goNext(&cIter);
		cIterData = LList_postfix_p_getData(&cIter);
		size_t leftChildIdx = Heap_postfix_obtainLeftmostChildIdx(parentIdx);
		size_t rightChildIdx = leftChildIdx + 1;
		if (cIterData == bufOfLastElmData)
		{
			if (leftChildIdx > heap->index)
			{
				/* break searching */
				break;
			}

			if (leftChildIdx == heap->index)
			{
				rightChildIdx = leftChildIdx;
			}
		}

		if (comparer(&cIterData[leftChildIdx], &cIterData[rightChildIdx]) < 0)
		{
			childIdx = leftChildIdx;
		}
		else
		{
			childIdx = rightChildIdx;
		}

		if (comparer(&cIterData[childIdx], &lastData) > 0)
		{
			break;
		}

		pIterData[parentIdx] = cIterData[childIdx];
		pIterData = cIterData;
		parentIdx = childIdx;
	}
	pIterData[parentIdx] = lastData;

	LList_postfix_p_iterUnref(&cIter);

	if (heap->index-- == 0)
	{
		heap->index +=
			heap->bound = Heap_postfix_obtainPrevBoundSize(heap->bound);
		LList_postfix_p_goPrev(&heap->bufOfLastElm);
	}

	return 1;
}
