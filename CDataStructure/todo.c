/* 할일을 관리하기 위한 라이브러리 */

/* Public 함수에 대한 문서는 todo.h에 있음 */


#include "todo.h"

#include <stdlib.h>
#include <string.h>

#include "../customdef.h"


void todo_initTodoManager(struct todo_TodoManager* m)
{
	LList_todo_init(&m->list);
}


ELListError todo_addTodo(struct todo_TodoManager* m, time_t start, time_t period, const char* detail)
{
	struct todo_Todo * added;

	if (!MALLOC_ASSIGN(added, 1))
	{
		return LLIST_NOMEM;
	}

	size_t len = strlen(detail) + 1;
	if (!MALLOC_ASSIGN(added->detail, len))
	{
		return LLIST_NOMEM;
	}

	strcpy_s(added->detail, len, detail);
	added->start = start;
	added->period = period;

	LList_todo_Iter iter; LList_todo_iterHead(&m->list, &iter);
	struct todo_Todo * t = NULL;
	while (LList_todo_goNext(&iter))
	{
		t = LList_todo_getData(&iter);
		if (added->start < t->start ||
			added->start == t->start && added->period < t->period)
		{
			break;
		}
	}
	ELListError result = LList_todo_addPrev(&iter, added);
	LList_todo_iterUnref(&iter);

	return result;
}


int todo_fromTodo(struct todo_TodoManager* m, struct todo_TodoIter* todoIter, time_t minStart)
{
	LList_todo_iterHead(&m->list, &todoIter->iter);
	struct todo_Todo * t = NULL;
	int result = 0;
	while (LList_todo_goNext(&todoIter->iter))
	{
		t = LList_todo_getData(&todoIter->iter);
		if (minStart <= t->start)
		{
			result = 1;
			break;
		}
	}

	return result;
}


int todo_untilTodo(struct todo_TodoIter* todoIter, time_t maxStart)
{
	if (!LList_todo_goNext(&todoIter->iter))
	{
		return 0;
	}

	struct todo_Todo * t = todo_getTodo(todoIter);
	
	return t->start <= maxStart;
}


void todo_iterUnref(struct todo_TodoIter* todoIter)
{
	LList_todo_iterUnref(&todoIter->iter);
}


struct todo_Todo* todo_getTodo(const struct todo_TodoIter* todoIter)
{
	return LList_todo_getData(&todoIter->iter);
}


size_t todo_delTodo(struct todo_TodoManager* m, time_t minStart, time_t maxStart)
{
	size_t count;

	struct todo_TodoIter todoIter;
	struct todo_Todo * t = NULL;
	count = 0;
	for (int i1 = todo_fromTodo(m, &todoIter, minStart);
		i1;
		i1 = todo_untilTodo(&todoIter, maxStart))
	{
		/* todo_addTodo에서 할당한 문자열 길이의 힙 해제 */
		LList_todo_removeGoPrev(&todoIter.iter, &t);
		free(t->detail);
		++count;
	}
	todo_iterUnref(&todoIter);

	return count;
}
