


#pragma once


#include "LinkedList.h"


typedef enum EHeapError
{
	/**
	 * It is guaranteed to be 0
	 */
	GRAPH_SUCCESS = 0,
	
	/**
	 * Failure because of lack of free memory
	 */
	GRAPH_NOMEM = INT_MIN >> 2,
} EHeapError;

typedef int type0;
typedef int type1;

typedef struct ListGraphVertex_postfix ListGraphVertex_postfix;
typedef struct ListGraphEdge_postfix ListGraphEdge_postfix;

DECLARE_LLIST(ListGraphEdge_postfix, ListGraphEdge_postfix*)

struct ListGraphVertex_postfix
{
	LList_ListGraphEdge_postfix llist;
	type0 data;
};

struct ListGraphEdge_postfix
{
	ListGraphVertex_postfix* from, * to;
	type1 data;
};



