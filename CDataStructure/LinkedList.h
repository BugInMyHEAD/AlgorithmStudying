/*
Copyright(c) 2018 BugInMyHEAD

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/


/**
 * Doubly linked circular list with iterator,
 * which has a dummy head node whose index is SIZE_MAX.
 *
 * Manipulation of a list is achived through exact specific number of iterator
 * referencing the list. The exact required number differs for each list
 * manipulating function. Excession of the number of iterators at that moment
 * leads the function not to work intentionally, but to return
 * 'LLIST_ANOTHER_ITER'.
 *
 * LinkedList.h에서 정의하는 library는 LList라는 접두어가 붙음
 *
 * Abbreviation:
 *  fwd = forward, rev = reverse
 *  abs = absolute, rel = relative
 *  prev = previous
 *
 * @see LLIST_ANOTHER_ITER
 */


/* 구현 및 private 함수, 기타 세부사항에 대한 문서는 LinkedList.c에 있음 */


#pragma once


#include <stddef.h>
#include <limits.h>

#include "../customdef.h"


#define LLIST_NUM_DUMMY 1


typedef enum ELListError
{
	/**
	 * It is guaranteed that LLIST_SUCCESS is 0
	 */\
	LLIST_SUCCESS = 0,\
	\
	/**
	 * Failure because of lack of free memory
	 */
	LLIST_NOMEM = INT_MIN >> 2,

	/**
	 * Failure to indexing a specific element
	 */
	LLIST_NO_SUCH_ELM,

	/**
	 * Failure because of existence of another iterator
	 */
	LLIST_ANOTHER_ITER,

	/**
	 * Failure because of disagreement of referencing LList
	 */
	LLIST_DISAGREE,
} ELListError;


#define DECLARE_LLIST(postfix, type) \
\
typedef struct LList_##postfix##_Node LList_##postfix##_Node;\
typedef struct LList_##postfix LList_##postfix;\
typedef struct LList_##postfix##_Iter LList_##postfix##_Iter;\
\
/**
 * Node structure that consists a list.
 */\
struct LList_##postfix##_Node\
{\
	LList_##postfix##_Node* prev, * next;\
	type data;\
};\
\
/**
 * 리스트 핵심 구조체
 */\
struct LList_##postfix\
{\
	/**
	 * Dummy node
	 */\
	LList_##postfix##_Node head;\
	\
	/**
	 * Number of elements
	 */\
	size_t size;\
	\
	/**
	 * Number of iterators referencing this LList_##postfix
	 */\
	int iterCount;\
};\
\
/**
 * 인덱스는 0(head node)부터 next 방향으로 1씩 증가
 * 마지막 노드의 인덱스는 list->size와 같음
 * 'First' 및 '첫 번째' 노드는 인덱스 1인 노드를 말함
 */\
struct LList_##postfix##_Iter\
{\
	LList_##postfix* list;\
	LList_##postfix##_Node* node;\
	size_t index;\
};\
\
\
/**
 * 리스트 구조체 초기화
 * 실패하지 않음
 */\
void LList_##postfix##_init(LList_##postfix* list);\
\
/**
 * 초기화된 리스트 동적 할당
 *
 * @return 동적 할당 실패 시 NULL, 성공 시 할당된 주소의 포인터
 */\
LList_##postfix* LList_##postfix##_mallocAndInit(void);\
\
/**
 * @return 리스트의 dummy head node를 제외한 요소의 개수
 */\
size_t LList_##postfix##_getSize(const LList_##postfix* list);\
\
/**
 * @return Number of `LList_##postfix##_Iter`s referencing this
 * LList_##postfix
 */\
int LList_##postfix##_getIterCount(const LList_##postfix* list);\
\
/**
 * 리스트 방향 전치(next를 prev로, 그 반대도)
 * To succeed, there must not be an iterator referencing this list
 *
 * @return LLIST_ANOTHER_ITER: failure because of existence of another iterator
 *         LLIST_SUCCESS: success
 */\
ELListError LList_##postfix##_invert(LList_##postfix##_Iter* iter);\
\
\
/**
 * Head node를 가리키는 이터레이터 동적 할당
 *
 * @return 동적 할당 실패 시 NULL, 성공 시 할당된 주소의 포인터
 */\
LList_##postfix##_Iter* LList_##postfix##_iterMallocIterHead(\
	LList_##postfix* list);\
\
/**
 * 이터레이터를 초기화하며 head node를 가리키게 함
 * 실패하지 않음
 */\
void LList_##postfix##_iterHead(\
	LList_##postfix* list, LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터를 초기화하며 head node를 가리키게 함
 * 실패하지 않음
 */\
void LList_##postfix##_copyIter(\
	LList_##postfix##_Iter* dest,\
	const LList_##postfix##_Iter* src);\
\
/**
 * Destruct LList_##postfix##_Iter
 *
 * Decrement iterCount of its referencing list
 *
 * @param iter  which is being destructed
 */\
void LList_##postfix##_iterUnref(LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터를 초기화하며 첫 번째 노드를 가리키게 함
 * 인덱스 범위를 벗어나는 경우(list->size == 0인 경우밖에 없음)
 * iter가 변경되지 않음
 *
 * LList_##postfix##_iterFwd(list, iter, 1)와 같음: 실패할 수 있음
 *
 * @return 인덱스 범위를 벗어나면(iter->size == 0) 0, 그 외에는 1을 반환
 */\
int LList_##postfix##_iterFirst(\
	LList_##postfix* list, LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터를 초기화하며 마지막 노드를 가리키게 함
 * 인덱스 범위를 벗어나는 경우(list->size == 0인 경우밖에 없음)
 * iter가 변경되지 않음
 *
 * LList_##postfix##_iterRev(list, iter, 1)와 같음: 실패할 수 있음
 *
 * @return 인덱스 범위를 벗어나면(iter->size == 0) 0, 그 외에는 1을 반환
 */\
int LList_##postfix##_iterLast(\
	LList_##postfix* list, LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터를 초기화하며 특정 인덱스의 노드를 가리키게 함
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * @param index head node(0)에서 next 방향으로의 위치
 * @return 인덱스 범위를 벗어나는 경우 0, 그 외에는 1을 반환
 */\
int LList_##postfix##_iterFwd(\
	LList_##postfix* list,\
	LList_##postfix##_Iter* iter,\
	size_t index);\
\
/**
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 * 이터레이터를 초기화하며 특정 인덱스의 노드를 가리키게 함
 *
 * @param index head node(0)에서 prev 방향으로의 위치
 * @return 인덱스 범위를 벗어나는 경우 0, 그 외에는 1을 반환
 */\
int LList_##postfix##_iterRev(\
	LList_##postfix* list,\
	LList_##postfix##_Iter* iter,\
	size_t index);\
\
\
/**
 * @return 1 if the iterator points the head node,
 *         0 otherwise.
 */\
int LList_##postfix##_isHead(const LList_##postfix##_Iter* iter);\
\
/**
 * @return The pointer of LList_##postfix the iterator references.
 */\
LList_##postfix* LList_##postfix##_getList(\
	const LList_##postfix##_Iter* iter);\
\
/**
 * Head node에서부터 next 방향으로의 인덱스
 *
 * @return iter->index
 */\
size_t LList_##postfix##_getIndex(const LList_##postfix##_Iter* iter);\
\
/**
 * Head node에서부터 prev 방향으로의 인덱스
 */\
size_t LList_##postfix##_getIndexRev(const LList_##postfix##_Iter* iter);\
\
/**
 * @return iter->node->data
 */\
type LList_##postfix##_getData(const LList_##postfix##_Iter* iter);\
\
/**
 *
 */\
int LList_##postfix##_getDataAt(\
	const LList_##postfix##_Iter* iter, size_t index, type* buffer);\
\
\
/**
 * 이터레이터가 head node를 가리키게 함
 * 실패하지 않음
 */\
void LList_##postfix##_goHead(LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터가 인접하는 다음 노드를 가리키게 함
 * 실패하지 않는 함수이며, 반환값은 head node인지 구별하기 위함
 *
 * @return 수행 후에 이터레이터가 가리키는 노드가 head node(index == 0)이면 0,
 *         그 외에는 1을 반환
 */\
int LList_##postfix##_goNext(LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터가 인접하는 이전 노드를 가리키게 함
 * 실패하지 않는 함수이며, 반환값은 head node인지 구별하기 위함
 *
 * @return 수행 후에 이터레이터가 가리키는 노드가 head node(index == 0)이면 0,
 *         그 외에는 1을 반환
 */\
int LList_##postfix##_goPrev(LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터가 첫 번째 노드를 가리키게 함
 * LList_##postfix##_goAbsFwd(iter, 0)과 같음: 실패할 수 있음
 *
 * 실패하지 않아야 하는 경우,
 * LList_##postfix##_goHead(LList_##postfix##_Iter*)와
 * LList_##postfix##_goNext(LList_##postfix##_Iter*)를 사용해야 함
 *
 * @return 인덱스 범위를 벗어나면(iter->size == 0) 0, 그 외에는 1을 반환
 *
 * @see int LList_##postfix##_goAbsFwd(
 *      LList_##postfix##_Iter* iter, size_t index)
 * @see void LList_##postfix##_goHead(LList_##postfix##_Iter* iter)
 * @see int LList_##postfix##_goNext(LList_##postfix##_Iter* iter)
 */\
int LList_##postfix##_goFirst(LList_##postfix##_Iter* iter);\
\
/**
 * 이터레이터가 마지막 노드를 가리키게 함
 * LList_##postfix##_goAbsRev(iter, 0)과 같음: 실패할 수 있음
 *
 * 실패하지 않아야 하는 경우,
 * LList_##postfix##_goHead(LList_##postfix##_Iter*)와
 * LList_##postfix##_goPrev(LList_##postfix##_Iter*)를 사용해야 함
 *
 * @return 인덱스 범위를 벗어나면(iter->size == 0) 0, 그 외에는 1을 반환
 *
 * @see int LList_##postfix##_goAbsRev(
 *      LList_##postfix##_Iter* iter, size_t index)
 * @see void LList_##postfix##_goHead(LList_##postfix##_Iter* iter)
 * @see int LList_##postfix##_goPrev(LList_##postfix##_Iter* iter)
 */\
int LList_##postfix##_goLast(LList_##postfix##_Iter* iter);\
\
\
/**
 * 이터레이터가 next 방향 인덱스의 노드를 가리키게 함
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * @return 1
 *         0
 */\
int LList_##postfix##_goAbsFwd(LList_##postfix##_Iter* iter, size_t index);\
\
/**
 * 이터레이터가 prev 방향 인덱스의 노드를 가리키게 함
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * @return 1
 *         0
 */\
int LList_##postfix##_goAbsRev(LList_##postfix##_Iter* iter, size_t index);\
\
/**
 * 이터레이터가 가리키는 현재의 노드에서 상대적인 위치의 노드를 가리키게 함
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * iter->index - offset 으로 절대(head node로부터의 인덱스) 위치를 구함
 *
 * @return 1
 *         0
 */\
int LList_##postfix##_goRelFwd(\
	LList_##postfix##_Iter* iter, size_t offset);\
\
/**
 * 이터레이터가 가리키는 현재의 노드에서 상대적인 위치의 노드를 가리키게 함
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * iter->index - offset 으로 절대(head node로부터의 인덱스) 위치를 구함
 *
 * @return 1
 *         0
 */\
int LList_##postfix##_goRelRev(\
	LList_##postfix##_Iter* iter, size_t offset);\
\
\
/**
 * 이터레이터의 현재 위치의 다음 위치에 새 노드 삽입
 *
 * @param iter 모든 속성(인덱스 포함)이 변하지 않음
 * @return LLIST_WRITER_EXCEEDS
 *         LLIST_NOMEM
 *         LLIST_SUCCESS
 */\
ELListError LList_##postfix##_addNext(\
	const LList_##postfix##_Iter* iter, type data);\
\
/**
 * 이터레이터의 현재 위치의 이전 위치에 새 노드 삽입
 *
 * @param iter 인덱스가 1 증가
 * @return LLIST_WRITER_EXCEEDS
 *         LLIST_NOMEM
 *         LLIST_SUCCESS
 */\
ELListError LList_##postfix##_addPrev(\
	LList_##postfix##_Iter* iter, type data);\
\
/**
 * 
 *
 * @param iter 인덱스가 1 증가
 * @return LLIST_WRITER_EXCEEDS
 *         LLIST_NOMEM
 *         LLIST_SUCCESS
 */\
ELListError LList_##postfix##_addFirst(\
	LList_##postfix##_Iter* iter, type data);\
\
/**
 * 
 *
 * @param iter 모든 속성(인덱스 포함)이 변하지 않음
 * @return LLIST_WRITER_EXCEEDS
 *         LLIST_NOMEM
 *         LLIST_SUCCESS
 */\
ELListError LList_##postfix##_addLast(\
	const LList_##postfix##_Iter* iter, type data);\
\
\
/**
 * 이터레이터의 현재 위치의 노드를 삭제하고 다음 노드를 가리키게 함
 */\
ELListError LList_##postfix##_removeGoNext(\
	LList_##postfix##_Iter* iter, type* buffer);\
\
/**
 * 이터레이터의 현재 위치의 노드를 삭제하고 이전 노드를 가리키게 함
 */\
ELListError LList_##postfix##_removeGoPrev(\
	LList_##postfix##_Iter* iter, type* buffer);\
\
/**
 * 
 */\
ELListError LList_##postfix##_move(\
	LList_##postfix##_Iter* paster,\
	LList_##postfix##_Iter* cutBegin,\
	LList_##postfix##_Iter* cutEnd);\



#define DEFINE_LLIST(postfix, type) \
\
/**
 * Chain two nodes
 */\
static void LList_##postfix##_chain(\
	LList_##postfix##_Node* prev, LList_##postfix##_Node* next)\
{\
	prev->next = next;\
	next->prev = prev;\
}\
\
\
/**
 * @return The list referenced by 'a' and by 'b',
 *         NULL if their referencing lists are different.
 */\
static LList_##postfix* LList_##postfix##_agree(\
	const LList_##postfix##_Iter* a, const LList_##postfix##_Iter* b)\
{\
	LList_##postfix* list = LList_##postfix##_getList(a);\
	if (list != LList_##postfix##_getList(b))\
	{\
		return NULL;\
	}\
\
	return list;\
}\
\
\
/**
 * @return The number of all nodes in the list including the head node
 */\
static size_t LList_##postfix##_getActualSize(const LList_##postfix* list)\
{\
	return LList_##postfix##_getSize(list) + LLIST_NUM_DUMMY;\
}\
\
\
void LList_##postfix##_init(LList_##postfix* list)\
{\
	/* To form a doubly linked circular list */\
	list->head.next = list->head.prev = &list->head;\
	list->iterCount = 0;\
	list->size = 0;\
}\
\
\
/**
 * 초기화된 LList_##postfix##_Node 동적 할당
 *
 * @return 동적 할당 실패 시 NULL, 성공 시 할당된 주소의 포인터
 */\
static LList_##postfix##_Node* LList_##postfix##_nodeMallocAndInit(\
	LList_##postfix##_Node* prev, LList_##postfix##_Node* next, type data)\
{\
	LList_##postfix##_Node* result;\
	if (MALLOC_ASSIGN(result, 1))\
	{\
		LList_##postfix##_chain(result, next);\
		LList_##postfix##_chain(prev, result);\
		result->data = data;\
	}\
\
	return result;\
}\
\
\
LList_##postfix* LList_##postfix##_mallocAndInit(void)\
{\
	LList_##postfix* result;\
	if (MALLOC_ASSIGN(result, 1))\
	{\
		LList_##postfix##_init(result);\
	}\
\
	return result;\
}\
\
\
size_t LList_##postfix##_getSize(const LList_##postfix* list)\
{\
	return list->size;\
}\
\
\
int LList_##postfix##_getIterCount(const LList_##postfix* list)\
{\
	return list->iterCount;\
}\
\
\
void LList_##postfix##_iterUnref(LList_##postfix##_Iter* iter)\
{\
	LList_##postfix##_getList(iter)->iterCount--;\
	iter->list = NULL;\
	iter->node = NULL;\
}\
\
\
void LList_##postfix##_iterHead(\
	LList_##postfix* list, LList_##postfix##_Iter* iter)\
{\
	iter->list = list;\
	iter->index = SIZE_MAX;\
	iter->node = &list->head;\
	list->iterCount++;\
}\
\
\
void LList_##postfix##_copyIter(\
	LList_##postfix##_Iter* dest, const LList_##postfix##_Iter* src)\
{\
	/*LList_##postfix* list = LList_##postfix##_agree(dest, src);\
	if (!list)\
	{\
		return LLIST_DISAGREE;\
	}*/\
\
	*dest = *src;\
	LList_##postfix##_getList(src)->iterCount++;\
\
	/*return LLIST_SUCCESS;*/\
}\
\
\
LList_##postfix##_Iter* LList_##postfix##_iterMallocIterHead(\
	LList_##postfix* list)\
{\
	LList_##postfix##_Iter* result;\
	if (MALLOC_ASSIGN(result, 1))\
	{\
		LList_##postfix##_iterHead(list, result);\
		list->iterCount++;\
	}\
\
	return result;\
}\
\
\
ELListError LList_##postfix##_invert(LList_##postfix##_Iter* iter)\
{\
	LList_##postfix* list = LList_##postfix##_getList(iter);\
\
	if (LList_##postfix##_getIterCount(list) != 1)\
	{\
		return LLIST_ANOTHER_ITER;\
	}\
\
	/**
	 * Head node부터 'prev' 방향으로(전치되었으므로)
	 * head node 직전 node까지
	 *
	 * iter.index의 변화는 신경쓰지 않는다
	 */\
	LList_##postfix##_Iter temp; LList_##postfix##_iterHead(list, &temp);\
	do\
	{\
		SWAP(temp.node->prev, temp.node->next);\
	} while (LList_##postfix##_goNext(&temp));\
\
	iter->index = LList_##postfix##_getIndexRev(iter);\
\
	return LLIST_SUCCESS;\
}\
\
\
int LList_##postfix##_iterFirst(\
	LList_##postfix* list, LList_##postfix##_Iter* iter)\
{\
	return LList_##postfix##_iterFwd(list, iter, 0);\
}\
\
\
int LList_##postfix##_iterLast(\
	LList_##postfix* list, LList_##postfix##_Iter* iter)\
{\
	return LList_##postfix##_iterRev(list, iter, 0);\
}\
\
\
/**
 * LList_##postfix##_iterFwd와 LList_##postfix##_iterRev의 공통 부분
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * @param LList_##postfix##_goAbs  LList_##postfix##_goAbsFwd 또는
 *        LList_##postfix##_goAbsRev
 * @return 인덱스 범위(0(head node)에서 iter->list->size)를 벗어나면 0,
 *         그렇지 않으면 1
 */\
static int LList_##postfix##_iter(\
	LList_##postfix* list,\
	LList_##postfix##_Iter* iter,\
	size_t index,\
	int(*llistGoAbs)(LList_##postfix##_Iter*, size_t))\
{\
	LList_##postfix##_Iter temp; LList_##postfix##_iterHead(list, &temp);\
	int result = llistGoAbs(&temp, index);\
	if (result)\
	{\
		*iter = temp;\
	}\
	else\
	{\
		LList_##postfix##_iterUnref(&temp);\
	}\
\
	return result;\
}\
\
\
int LList_##postfix##_iterFwd(\
	LList_##postfix* list, LList_##postfix##_Iter* iter, size_t index)\
{\
	return LList_##postfix##_iter(\
		list, iter, index, LList_##postfix##_goAbsFwd);\
}\
\
\
int LList_##postfix##_iterRev(\
	LList_##postfix* list, LList_##postfix##_Iter* iter, size_t index)\
{\
	return LList_##postfix##_iter(\
		list, iter, index, LList_##postfix##_goAbsRev);\
}\
\
\
/**
 * 이터레이터가 head node를 가리키는지 확인
 *
 * @return iter가 head node를 가리키면 1, 그렇지 않으면 0
 */\
int LList_##postfix##_isHead(const LList_##postfix##_Iter* iter)\
{\
	return iter->node == &LList_##postfix##_getList(iter)->head;\
}\
\
\
/**
 * LList_##postfix##_goNext와 LList_##postfix##_goPrev의 공통 부분
 * 이터레이터가 인접하는 다음 혹은 이전 노드를 가리키게 함
 * 실패하지 않는 함수이며, 반환값은 head node인지 구별하기 위함
 *
 * @param offset  prev 방향 함수이면 -1, next 방향 함수이면 +1
 *                Undefined behavior if offset is neither -1 nor +1
 * @return 수행 후에 이터레이터가 가리키는 노드가 head node(index == 0)이면 0,
 *         그 외에는 1을 반환
 */\
static int LList_##postfix##_goAdj(\
	LList_##postfix##_Iter* iter,\
	LList_##postfix##_Node* node,\
	size_t offset)\
{\
	iter->node = node;\
	size_t aSize =\
		LList_##postfix##_getActualSize(LList_##postfix##_getList(iter));\
	int result = !LList_##postfix##_isHead(iter);\
	iter->index =\
		( iter->index + LLIST_NUM_DUMMY + offset + aSize ) % aSize\
		- LLIST_NUM_DUMMY;\
\
	return result;\
}\
\
\
int LList_##postfix##_goNext(LList_##postfix##_Iter* iter)\
{\
	return LList_##postfix##_goAdj(iter, iter->node->next, +1);\
}\
\
\
int LList_##postfix##_goPrev(LList_##postfix##_Iter* iter)\
{\
	return LList_##postfix##_goAdj(iter, iter->node->prev, -1);\
}\
\
\
/**
 * LList_##postfix##_goAbsFwd와 LList_##postfix##_goAbsRev,
 * LList_##postfix##_goRelFwd, LList_##postfix##_goRelRev의 공통 부분
 * 인덱스 범위를 벗어나는 경우 iter가 변경되지 않음
 *
 * @param rev  prev 방향 함수이면 1, next 방향 함수이면 0
 * @return 1
 *         0
 */\
static int LList_##postfix##_goAbs(\
	LList_##postfix##_Iter* iter, size_t index, int rev)\
{\
	size_t size = LList_##postfix##_getSize(LList_##postfix##_getList(iter));\
	/* 'Go'ing to the head node by this function is not allowed */\
	if (index >= size)\
	{\
		return 0;\
	}\
\
	size_t pastIndex = LList_##postfix##_getIndex(iter);\
	/* 노드 이동 경로 최적화
	 *
	 * iter->node로부터 출발하는 것이 빠른지,
	 * head node로부터 출발하는 것이 빠른지 평가
	 *
	 * absolute는 head node로부터의 거리,
	 * relative는 iter->node로부터의 거리임
	 * offset에 위 둘 중에 짧은 거리를 배정
	 *
	 * absolute, relative, rev에 의해 이동 방향 결정(llistGoAdj) */\
	size_t absolute, relative;\
	size_t offset;\
	int(*llistGoAdj)(LList_##postfix##_Iter*);\
	if (rev)\
	{\
		relative = pastIndex - index;\
		absolute = index - SIZE_MAX;\
		if (absolute >= relative)\
		{\
			llistGoAdj = LList_##postfix##_goPrev;\
			offset = relative;\
		}\
		else\
		{\
			LList_##postfix##_goHead(iter);\
			llistGoAdj = LList_##postfix##_goNext;\
			offset = absolute;\
		}\
	}\
	else\
	{\
		relative = index - pastIndex;\
		absolute = size - index;\
		if (relative <= absolute)\
		{\
			llistGoAdj = LList_##postfix##_goNext;\
			offset = relative;\
		}\
		else\
		{\
			LList_##postfix##_goHead(iter);\
			llistGoAdj = LList_##postfix##_goPrev;\
			offset = absolute;\
		}\
	}\
	for (size_t i1 = 0; i1 < offset; ++i1)\
	{\
		llistGoAdj(iter);\
	}\
\
	return 1;\
}\
\
\
int LList_##postfix##_goAbsFwd(LList_##postfix##_Iter* iter, size_t index)\
{\
	return LList_##postfix##_goAbs(iter, index, 0);\
}\
\
\
int LList_##postfix##_goAbsRev(LList_##postfix##_Iter* iter, size_t index)\
{\
	return LList_##postfix##_goAbs(iter, index, 1);\
}\
\
\
int LList_##postfix##_goRelFwd(\
	LList_##postfix##_Iter* iter, size_t offset)\
{\
	return LList_##postfix##_goAbsFwd(\
		iter, LList_##postfix##_getIndex(iter) + offset);\
}\
\
\
int LList_##postfix##_goRelRev(\
	LList_##postfix##_Iter* iter, size_t offset)\
{\
	return LList_##postfix##_goAbsFwd(\
		iter, LList_##postfix##_getIndex(iter) - offset);\
}\
\
\
void LList_##postfix##_goHead(LList_##postfix##_Iter* iter)\
{\
	iter->index = SIZE_MAX;\
	iter->node = &LList_##postfix##_getList(iter)->head;\
}\
\
\
int LList_##postfix##_goFirst(LList_##postfix##_Iter* iter)\
{\
	return LList_##postfix##_goAbsFwd(iter, 0);\
}\
\
\
int LList_##postfix##_goLast(LList_##postfix##_Iter* iter)\
{\
	return LList_##postfix##_goAbsRev(iter, 0);\
}\
\
\
type LList_##postfix##_getData(const LList_##postfix##_Iter* iter)\
{\
	return iter->node->data;\
}\
\
\
int LList_##postfix##_getDataAt(\
	const LList_##postfix##_Iter* iter, size_t index, type* buffer)\
{\
	LList_##postfix##_Iter tempIter = *iter;\
	int result = LList_##postfix##_goAbsFwd(&tempIter, index);\
	if (result)\
	{\
		*buffer = LList_##postfix##_getData(&tempIter);\
	}\
\
	return result;\
}\
\
\
LList_##postfix* LList_##postfix##_getList(\
	const LList_##postfix##_Iter* iter)\
{\
	return iter->list;\
}\
\
\
size_t LList_##postfix##_getIndex(const LList_##postfix##_Iter* iter)\
{\
	return iter->index;\
}\
\
\
size_t LList_##postfix##_getIndexRev(const LList_##postfix##_Iter* iter)\
{\
	LList_##postfix* list = LList_##postfix##_getList(iter);\
	size_t aSize = LList_##postfix##_getActualSize(list);\
	size_t index = LList_##postfix##_getIndex(iter);\
\
	return ( aSize - index - 1 ) % aSize - LLIST_NUM_DUMMY;\
}\
\
\
/**
 * LList_##postfix##_addNext와 LList_##postfix##_addPrev의 공통 부분
 *
 * prev와 next 사이에 새 노드가 끼워넣어짐
 *
 * @return LLIST_WRITER_EXCEEDS
 *         LLIST_NOMEM
 *         LLIST_SUCCESS
 */\
static ELListError LList_##postfix##_add(\
	LList_##postfix* list,\
	type data,\
	LList_##postfix##_Node* prev,\
	LList_##postfix##_Node* next)\
{\
	if (LList_##postfix##_getIterCount(list) != 1)\
	{\
		return LLIST_ANOTHER_ITER;\
	}\
\
	LList_##postfix##_Node* node =\
		LList_##postfix##_nodeMallocAndInit(prev, next, data);\
	if (!node)\
	{\
		return LLIST_NOMEM;\
	}\
\
	list->size++;\
\
	return LLIST_SUCCESS;\
}\
\
\
ELListError LList_##postfix##_addNext(\
	const LList_##postfix##_Iter* iter, type data)\
{\
	LList_##postfix* list = LList_##postfix##_getList(iter);\
\
	return LList_##postfix##_add(list, data, iter->node, iter->node->next);\
}\
\
\
ELListError LList_##postfix##_addLast(\
	const LList_##postfix##_Iter* iter, type data)\
{\
	LList_##postfix* list = LList_##postfix##_getList(iter);\
\
	return LList_##postfix##_add(list, data, list->head.prev, &list->head);\
}\
\
\
static ELListError LList_##postfix##_addPrevOrFirst(\
	LList_##postfix##_Iter* iter,\
	LList_##postfix* list,\
	type data,\
	LList_##postfix##_Node* prev,\
	LList_##postfix##_Node* next)\
{\
	ELListError result = LList_##postfix##_add(list, data, prev, next);\
	if (result == LLIST_SUCCESS && !LList_##postfix##_isHead(iter))\
	{\
		iter->index++;\
	}\
\
	return result;\
}\
\
\
ELListError LList_##postfix##_addPrev(\
	LList_##postfix##_Iter* iter, type data)\
{\
	LList_##postfix* list = LList_##postfix##_getList(iter);\
\
	return LList_##postfix##_addPrevOrFirst(\
		iter, list, data, iter->node->prev, iter->node);\
}\
\
\
ELListError LList_##postfix##_addFirst(\
	LList_##postfix##_Iter* iter, type data)\
{\
	LList_##postfix* list = LList_##postfix##_getList(iter);\
\
	return LList_##postfix##_addPrevOrFirst(\
		iter, list, data, &list->head, list->head.next);\
}\
\
\
/**
 * LList_##postfix##_removeGoNext와 LList_##postfix##_removeGoPrev의 공통 부분
 *
 * @param llistGoAdj  LList_##postfix##_goNext 또는 LList_##postfix##_goPrev
 */\
static ELListError LList_##postfix##_removeGo(\
	LList_##postfix##_Iter* iter,\
	type* buffer,\
	int(*llistGoAdj)(LList_##postfix##_Iter*))\
{\
	LList_##postfix* list = LList_##postfix##_getList(iter);\
\
	if (LList_##postfix##_getIterCount(list) != 1)\
	{\
		return LLIST_ANOTHER_ITER;\
	}\
\
	LList_##postfix##_chain(iter->node->prev, iter->node->next);\
\
	LList_##postfix##_Node* removed = iter->node;\
	if (buffer)\
	{\
		*buffer = removed->data;\
	}\
	llistGoAdj(iter);\
\
	free(removed);\
\
	list->size--;\
\
	return LLIST_SUCCESS;\
}\
\
\
ELListError LList_##postfix##_removeGoNext(\
	LList_##postfix##_Iter* iter, type* buffer)\
{\
	iter->index--;\
\
	return LList_##postfix##_removeGo(iter, buffer, LList_##postfix##_goNext);\
}\
\
\
ELListError LList_##postfix##_removeGoPrev(\
	LList_##postfix##_Iter* iter, type* buffer)\
{\
	return LList_##postfix##_removeGo(iter, buffer, LList_##postfix##_goPrev);\
}\
\
\
ELListError LList_##postfix##_move(\
	LList_##postfix##_Iter* paster,\
	LList_##postfix##_Iter* cutBeginInc,\
	LList_##postfix##_Iter* cutEndExc)\
{\
	LList_##postfix* pasted = LList_##postfix##_getList(paster);\
	LList_##postfix* cut =\
		LList_##postfix##_agree(cutBeginInc, cutEndExc);\
	if (!cut)\
	{\
		return LLIST_DISAGREE;\
	}\
\
	if (LList_##postfix##_getIterCount(pasted) != 1 ||\
		LList_##postfix##_getIterCount(cut) != 2)\
	{\
		return LLIST_ANOTHER_ITER;\
	}\
\
	size_t beginIdx = cutBeginInc->index;\
	size_t endIdx = cutEndExc->index;\
	size_t qty;\
	if (beginIdx != endIdx)\
	{\
		if (beginIdx > endIdx)\
		{\
			LList_##postfix##_chain(cut->head.prev, cut->head.next);\
			LList_##postfix##_chain(cutBeginInc->node->prev, &cut->head);\
			LList_##postfix##_chain(&cut->head, cutEndExc->node);\
			qty = beginIdx - endIdx - 1;\
			cutEndExc->index = 0;\
		}\
		else\
		{\
			LList_##postfix##_chain(cutBeginInc->node->prev, cutEndExc->node);\
			qty = endIdx - beginIdx;\
			cutEndExc->index -= qty;\
		}\
		LList_##postfix##_chain(paster->node->prev, cutBeginInc->node);\
		LList_##postfix##_chain(paster->node, cutEndExc->node->prev);\
		cut->size -= qty;\
		pasted->size += qty;\
		if (!LList_##postfix##_isHead(paster))\
		{\
			paster->index += qty;\
		}\
		cutBeginInc = cutEndExc;\
	}\
\
	return LLIST_SUCCESS;\
}\

