/**
 * Binary Search Tree
 *
 * BinarySearchTree.c에서 정의하는 함수는 BST라는 접두어가 붙음
 *
 * Public 함수에 대한 문서는 BinarySearchTree.h에 있음
 */


#include "../customdef.h"

#include "BinarySearchTree.h"


int(* Bst_postfix_getComparer(const Bst_postfix* bst))(const void*, const void*)
{
	return bst->comparer;
}


static size_t BstNode_postfix_getNumDescendant(BstNode_postfix* node)
{
	return node->numDescendant;
}


static size_t BstNode_postfix_getSize(BstNode_postfix* node)
{
	return node ? BstNode_postfix_getNumDescendant(node) + 1 : 0;
}


static size_t BstNode_postfix_getWeight(BstNode_postfix* node)
{
	return BstNode_postfix_getSize(node) + 1;
}


void Bst_postfix_construct(
	Bst_postfix* bst,
	int(* comparer)(const void*, const void*))
{
	bst->root = NULL;
	bst->comparer = comparer;
}


void Bst_postfix_destruct(Bst_postfix* bst)
{
	FREE_ASSIGN(bst->root);
}


static BstNode_postfix* Bst_postfix_makeNode(void)
{
	BstNode_postfix* node;
	if (!MALLOC_ASSIGN(node, 1))
	{
		return NULL;
	}

	BinaryTree_postfix_initNode(&node->binaryTreeNode);

	return node;
}


static void Bst_postfix_destroyNode(BstNode_postfix* node)
{
	free(node);
}


/**
 *
 *
 * @return node에 저장되어 있는 BinaryTreeData
 */
static BinaryTreeData BstNode_postfix_getData(const BstNode_postfix* node)
{
	return BinaryTree_postfix_getData(&node->binaryTreeNode);
}


/**
*
*
* @return node에 저장되어 있는 BinaryTreeData
*/
static BinaryTreeData* BstNode_postfix_getDataPtr(BstNode_postfix* node)
{
	return BinaryTree_postfix_getDataPtr(&node->binaryTreeNode);
}


/**
 * node에 저장되어 있는 BinaryTreeData를 교체
 */
void BstNode_postfix_setData(BstNode_postfix* node, BinaryTreeData data)
{
	BinaryTree_postfix_setData(&node->binaryTreeNode, data);
}


/**
 *
 *
 * @return 왼쪽 자식 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_getLeft(const BstNode_postfix* node)
{
	return WHOSE(
		BstNode_postfix,
		binaryTreeNode,
		BinaryTree_postfix_getLeft(&node->binaryTreeNode));
}


/**
 *
 *
 * @return 오른쪽 자식 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_getRight(const BstNode_postfix* node)
{
	return WHOSE(
		BstNode_postfix,
		binaryTreeNode,
		BinaryTree_postfix_getRight(&node->binaryTreeNode));
}


/**
 *
 *
 * @return 부모 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_getParent(const BstNode_postfix* node)
{
	return WHOSE(
		BstNode_postfix,
		binaryTreeNode,
		BinaryTree_postfix_getParent(&node->binaryTreeNode));
}


/**
 *
 *
 * @return 가장 왼쪽에 있는 노드의 주소(node 포함)
 */
static BstNode_postfix* BstNode_postfix_getLeftmost(BstNode_postfix* node)
{
	return WHOSE(
		BstNode_postfix,
		binaryTreeNode,
		BinaryTree_postfix_getLeftmost(&node->binaryTreeNode));
}


/**
 *
 *
 * @return 가장 오른쪽에 있는 노드의 주소(node 포함)
 */
static BstNode_postfix* BstNode_postfix_getRightmost(BstNode_postfix* node)
{
	return WHOSE(
		BstNode_postfix,
		binaryTreeNode,
		BinaryTree_postfix_getRightmost(&node->binaryTreeNode));
}


/**
 * 왼쪽 서브 트리 교체
 *
 * @param parent NULL이면 child가 루트 노드가 됨
 * @param child NULL이면 parent의 오른쪽 자식 노드가 NULL이 됨
 * @return 분리된 트리의 루트 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_changeLeft(
	BstNode_postfix* parent, BstNode_postfix* child)
{
	return WHOSE(
		BstNode_postfix,
		binaryTreeNode,
		BinaryTree_postfix_changeLeft(
			&parent->binaryTreeNode, &child->binaryTreeNode));
}


/**
 * 오른쪽 서브 트리 교체
 *
 * @param parent NULL이면 child가 루트 노드가 됨
 * @param child NULL이면 parent의 오른쪽 자식 노드가 NULL이 됨
 * @return 분리된 트리의 루트 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_changeRight(
	BstNode_postfix* parent, BstNode_postfix* child)
{
	return WHOSE(
		BstNode_postfix,
		binaryTreeNode,
		BinaryTree_postfix_changeRight(
			&parent->binaryTreeNode, &child->binaryTreeNode));
}


static void BstNode_postfix_switch(BstNode_postfix* a, BstNode_postfix* b)
{
	SWAP(a->numDescendant, b->numDescendant);
	BinaryTree_postfix_swap(&a->binaryTreeNode, &b->binaryTreeNode);
}


/**
 *
 *
 * @return 루트 노드이면 1, 그렇지 않으면 0
 */
static int BstNode_postfix_isRoot(const BstNode_postfix* node)
{
	return BinaryTree_postfix_isRoot(&node->binaryTreeNode);
}


/**
 *
 *
 * @return 리프 노드이면 1, 그렇지 않으면 0
 */
static int BstNode_postfix_isLeaf(const BstNode_postfix* node)
{
	return BinaryTree_postfix_isLeaf(&node->binaryTreeNode);
}


/**
 * For weight balanced tree
 * Reference: http://www.cs.toronto.edu/~trebla/CSCB63-2018-Summer/WBT.pdf
 * Bottom-up recursive function
 */
static void BstNode_postfix_balance(BstNode_postfix* node)
{
	if (node == NULL)
	{
		return;
	}

	/* To check the node is balanced */
	BstNode_postfix* ln = BstNode_postfix_getLeft(node);
	BstNode_postfix* rn = BstNode_postfix_getRight(node);
	size_t lw = BstNode_postfix_getWeight(ln);
	size_t rw = BstNode_postfix_getWeight(rn);
	BstNode_postfix* nextRoot;
	BstNode_postfix*(* nextFunc)(BstNode_postfix*);
	BstNode_postfix* cln, * crn;
	size_t clw, crw;
	if (lw > 3 * rw)
	{

	}
	else if (3 * lw < rw)
	{
		cln = BstNode_postfix_getLeft(rn);
		crn = BstNode_postfix_getRight(rn);
		clw = BstNode_postfix_getWeight(cln);
		crw = BstNode_postfix_getWeight(crn);
		/* Single rotation */
		if (clw < 2 * crw)
		{

		}
		/* Double rotation */
		else
		{

		}
	}

	BstNode_postfix_balance(BstNode_postfix_getParent(node));
}


static EBinaryTreeError Bst_postfix_insert(
	Bst_postfix* bst, BinaryTreeData data)
{
	return BINARY_TREE_SUCCESS;
}


EBinaryTreeError Bst_postfix_insertDup(Bst_postfix* bst, BinaryTreeData data)
{
	int(* comparer)(const void*, const void*) = Bst_postfix_getComparer(bst);

	BstNode_postfix* parent;
	BstNode_postfix* child;
	int compareResult;
	
	/* 새로운 노드가 추가될 위치를 찾음 */
	parent = NULL;
	child = bst->root;
	while (child)
	{
		compareResult = comparer(&data, BstNode_postfix_getDataPtr(child));

		parent = child;
		child =
			( compareResult < 0 ?
				BstNode_postfix_getLeft : BstNode_postfix_getRight )(child);
	}

	/* 새 노드 동적 할당 */
	child = Bst_postfix_makeNode();
	if (!child)
	{
		return BINARY_TREE_NOMEM;
	}
	BstNode_postfix_setData(child, data);

	/* 새 노드가 루트가 아니면 */
	if (parent)
	{
		( compareResult < 0 ?
			BstNode_postfix_changeLeft :
			BstNode_postfix_changeRight )(parent, child);
	}
	/* 새 노드가 루트이면 */
	else
	{
		bst->root = child;
	}

	BstNode_postfix_balance(parent);

	return BINARY_TREE_SUCCESS;
}


EBinaryTreeError Bst_postfix_insertUni(Bst_postfix* bst, BinaryTreeData data)
{
	return BINARY_TREE_SUCCESS;
}


BinaryTreeNode_postfix* Bst_postfix_delete(Bst_postfix* bst, BinaryTreeData target)
{
	int(* comparer)(const void*, const void*) = Bst_postfix_getComparer(bst);

	/* target에 대한 이진 트리 탐색 */
	BinaryTreeNode_postfix* deleted;
	deleted = bst->root;
	while (1)
	{
		/* target이 존재하지 않는 경우 */
		if(!deleted)
		{
			return NULL;
		}

		int compareResult = comparer(&target, &deleted->data);

		/* target을 찾았음 */
		if (compareResult == 0)
		{
			break;
		}

		deleted =
			( compareResult < 0 ?
				BinaryTree_postfix_getLeft :
				BinaryTree_postfix_getRight )(deleted);
	}

	/* deleted의 오른쪽 서브트리 중 가장 왼쪽에 있는 노드 */
	BinaryTreeNode_postfix* promoted =
		BinaryTree_postfix_getLeftmost(BinaryTree_postfix_getRight(deleted));
	BinaryTreeNode_postfix* parent;
	/* deleted의 오른쪽 서브트리가 있으면 */
	if (promoted)
	{
		/* promoted는 자신의 오른쪽 서브트리를 부모에 붙여놓고 분리됨 */
		parent = BinaryTree_postfix_getParent(promoted);
		BinaryTreeNode_postfix* right = BinaryTree_postfix_getRight(promoted);
		( parent == deleted ?
			BinaryTree_postfix_changeRight :
			BinaryTree_postfix_changeLeft )(parent, right);
		/* promoted가 delete를 대체하면서 deleted가 분리됨 */
		deleted->data = promoted->data;
		deleted = promoted;
	}
	/* deleted의 오른쪽 서브트리가 없으면 */
	else
	{
		parent = BinaryTree_postfix_getParent(deleted);
		BinaryTreeNode_postfix* left = BinaryTree_postfix_getLeft(deleted);
		if (BinaryTree_postfix_getLeft(parent) == deleted)
		{
			BinaryTree_postfix_changeLeft(parent, left);
		}
		if (BinaryTree_postfix_getRight(parent) == deleted)
		{
			BinaryTree_postfix_changeRight(parent, left);
		}
	}

	BstNode_postfix_balance(parent);

	return deleted;
}


int Bst_postfix_traverse(
	const Bst_postfix* bst,
	int(* binaryTree_trav)(const BinaryTreeNode_postfix*, void(*)(BinaryTreeData)),
	void(* action)(BinaryTreeData))
{
	return binaryTree_trav(bst->root, action);
}
