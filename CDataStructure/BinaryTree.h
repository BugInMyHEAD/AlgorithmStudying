/**
 * Binary Tree
 *
 * 구현 및 private 함수, 기타 세부사항에 대한 문서는
 * BinaryTree.c에 있음
 */


#pragma once


#include <stddef.h>


typedef enum EBinaryTreeError
{
	/**
	* Failure because of lack of free memory
	*/
	BINARY_TREE_NOMEM = -1,

	/**
	* It is guaranteed that BINARY_TREE_SUCCESS is 0
	*/
	BINARY_TREE_SUCCESS,
} EBinaryTreeError;
static_assert(!BINARY_TREE_SUCCESS, "BINARY_TREE_SUCCESS has to be 0");

typedef int BinaryTreeData;


/**
 * 이진 트리의 노드
 */
typedef struct BinaryTreeNode BinaryTreeNode_postfix;
struct BinaryTreeNode
{
	BinaryTreeNode_postfix* parent, * left, * right;
	BinaryTreeData data;
};

void BinaryTree_postfix_initNode(BinaryTreeNode_postfix* node);

/**
 *
 *
 * @return 동적 할당된 BinaryTreeNode, 동적 할당 실패 시 NULL
 */
BinaryTreeNode_postfix* BinaryTree_postfix_makeNode(void);

/**
 * BinaryTreeNode 소멸
 */
void BinaryTree_postfix_destroyNode(BinaryTreeNode_postfix* node);

/**
 *
 *
 * @return node에 저장되어 있는 BinaryTreeData
 */
BinaryTreeData BinaryTree_postfix_getData(
	const BinaryTreeNode_postfix* node);

/**
*
*
* @return node에 저장되어 있는 BinaryTreeData
*/
BinaryTreeData* BinaryTree_postfix_getDataPtr(
	BinaryTreeNode_postfix* node);

/**
 * node에 저장되어 있는 BinaryTreeData를 교체
 */
void BinaryTree_postfix_setData(
	BinaryTreeNode_postfix* node, BinaryTreeData data);

/**
 *
 *
 * @return 왼쪽 자식 노드의 주소
 */
BinaryTreeNode_postfix* BinaryTree_postfix_getLeft(
	const BinaryTreeNode_postfix* node);

/**
 *
 *
 * @return 오른쪽 자식 노드의 주소
 */
BinaryTreeNode_postfix* BinaryTree_postfix_getRight(
	const BinaryTreeNode_postfix* node);

/**
 *
 *
 * @return 부모 노드의 주소
 */
BinaryTreeNode_postfix* BinaryTree_postfix_getParent(
	const BinaryTreeNode_postfix* node);

/**
 *
 *
 * @return 가장 왼쪽에 있는 노드의 주소(node 포함)
 */
BinaryTreeNode_postfix* BinaryTree_postfix_getLeftmost(
	BinaryTreeNode_postfix* node);

/**
 *
 *
 * @return 가장 오른쪽에 있는 노드의 주소(node 포함)
 */
BinaryTreeNode_postfix* BinaryTree_postfix_getRightmost(
	BinaryTreeNode_postfix* node);

/**
 * 왼쪽 서브 트리 교체
 *
 * @param parent NULL이면 child가 루트 노드가 됨
 * @param child NULL이면 parent의 오른쪽 자식 노드가 NULL이 됨
 * @return 분리된 트리의 루트 노드의 주소
 */
BinaryTreeNode_postfix* BinaryTree_postfix_changeLeft(
	BinaryTreeNode_postfix* parent, BinaryTreeNode_postfix* child);

/**
 * 오른쪽 서브 트리 교체
 *
 * @param parent NULL이면 child가 루트 노드가 됨
 * @param child NULL이면 parent의 오른쪽 자식 노드가 NULL이 됨
 * @return 분리된 트리의 루트 노드의 주소
 */
BinaryTreeNode_postfix* BinaryTree_postfix_changeRight(
	BinaryTreeNode_postfix* parent, BinaryTreeNode_postfix* child);


void BinaryTree_postfix_swap(
	BinaryTreeNode_postfix* a, BinaryTreeNode_postfix* b);

/**
 *
 *
 * @return 루트 노드이면 1, 그렇지 않으면 0
 */
int BinaryTree_postfix_isRoot(const BinaryTreeNode_postfix* node);

/**
 *
 *
 * @return 리프 노드이면 1, 그렇지 않으면 0
 */
int BinaryTree_postfix_isLeaf(const BinaryTreeNode_postfix* node);

/**
 * 전위 순회
 *
 * @param action 순회 시 수행될 행동
 * @return 순회 노드의 수
 */
size_t BinaryTree_postfix_travPre(
	const BinaryTreeNode_postfix* bt, void(* action)(BinaryTreeData));

/**
 * 중위 순회
 *
 * @param action 순회 시 수행될 행동
 * @return 순회 노드의 수
 */
size_t BinaryTree_postfix_travIn(
	const BinaryTreeNode_postfix* bt, void(* action)(BinaryTreeData));

/**
 * 후위 순회
 *
 * @param action 순회 시 수행될 행동
 * @return 순회 노드의 수
 */
size_t BinaryTree_postfix_travPost(
	const BinaryTreeNode_postfix* bt, void(* action)(BinaryTreeData));

/**
 * 리프 노드 순회
 *
 * @param action 순회 시 수행될 행동
 * @return 순회 노드의 수
 */
size_t BinaryTree_postfix_travLeaf(
	const BinaryTreeNode_postfix* node, void(* action)(BinaryTreeData));
