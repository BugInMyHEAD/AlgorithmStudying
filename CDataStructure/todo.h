/* 할일을 관리하기 위한 라이브러리 */


#pragma once


#include <time.h>

#include "!declaration.h"


/**
 * 할일을 추상화한 구조체
 */
struct todo_Todo
{
	time_t start;
	time_t period;
	char * detail;
};

/**
 * 할일을 관리하는 구조체
 */
struct todo_TodoManager
{
	LList_todo list;
};

/**
 * 할일 리스트의 이터레이터
 */
struct todo_TodoIter
{
	LList_todo_Iter iter;
};

/**
* struct todo_TodoManager 초기화
* 실패하지 않음
*/
void todo_initTodoManager(struct todo_TodoManager* m);

/**
 * 할일 추가
 * 시작 시간과 실행 기간에 맞춰 정렬하여 추가한다
 *
 * @param detail 할일에 대한 설명 문자열이며, 그 길이만큼 동적할당하여 저장한다
 * @return 동적 할당 실패 시 0, 성공 시 1을 반환
 */
ELListError todo_addTodo(struct todo_TodoManager* m, time_t start, time_t period, const char* detail);

/**
 * 할일 삭제
 *
 * @return 삭제한 할일의 수
 */
size_t todo_delTodo(struct todo_TodoManager* m, time_t minStart, time_t maxStart);

/**
 * todoIter가 minStart보다 같거나 늦게 시작하는 할일을 가리키게 한다
 *
 * @return 하나도 없으면 0, 그외에는 1
 */
int todo_fromTodo(struct todo_TodoManager* m, struct todo_TodoIter* todoIter, time_t minStart);

/**
* todoIter가 maxStart보다 같거나 일찍 시작하는 할일을 가리키게 한다
*
* @return 찾으면 0, 그외에는 1
*/
int todo_untilTodo(struct todo_TodoIter* todoIter, time_t maxStart);

void todo_iterUnref(struct todo_TodoIter* todoIter);

/**
 * todoIter가 가리키는 할일을 반환한다
 */
struct todo_Todo* todo_getTodo(const struct todo_TodoIter* todoIter);
