#include <stdio.h>
#include <conio.h>
#include <limits.h>
#include <stdlib.h>
#include "todo.h"
#include "../customdef.h"


const int arr[] = { 10, 15, 8, 18, 7, 1, 5, 9, 17, 11, 12, 30, 25, 6, 32, };


int intComparer(const void* a, const void* b)
{
	int aa = *(const int*)a;
	int bb = *(const int*)b;
	switch (( aa < bb ) << 1 | ( aa > bb ) << 0)
	{
	case 0b00:
		return 0;
	case 0b01:
		return 1;
	case 0b10:
		return -1;
	default:
		exit(6);
	}
}


int main(void)
{
	/* Heap */

	Heap_postfix heap; Heap_postfix_construct(&heap, intComparer);

	for (size_t i1 = 0; i1 < ARRLEN(arr); ++i1)
	{
		Heap_postfix_insert(&heap, arr[i1]);
	}

	/* Heap_delete는 Heap이 비어있으면 0을 반환 */
	for (int i1; Heap_postfix_delete(&heap, &i1); )
	{
		printf("%d ", i1);
	}
	puts("");

	Heap_postfix_destruct(&heap);


	/* LList */

	struct todo_TodoManager todoMan; todo_initTodoManager(&todoMan);
	
	for (; ; )
	{
		printf("TODO list\n");
		printf("1. add\n");
		printf("2. delete\n");
		printf("3. query\n");
		printf("4. exit\n");

		struct tm calendar;
		time_t start, period, end;
		struct todo_TodoIter todoIter;
		char buf[BUFSIZ];
		int i2 = _getch();
		_getch();
		
		/* struct tm에서 쓰이지 않는 멤버들 0으로 초기화 */
		calendar.tm_isdst = 0;
		calendar.tm_sec = 0;
		calendar.tm_wday = 0;
		calendar.tm_yday = 0;
		switch (i2)
		{
		case '1':
			/* 할일 추가하기 */
			printf("adding todo...\n");
			/* 할일의 시작 시간을 아래의 패턴으로 입력받아 time_t로 저장*/
			printf("start(YYYY/MM/DD hh:mm):");
			scanf_s(" %d%*[-/: ] ", &calendar.tm_year);
			calendar.tm_year -= 1900;
			scanf_s(" %d%*[-/: ] ", &calendar.tm_mon);
			--calendar.tm_mon;
			scanf_s(" %d%*[-/: ] ", &calendar.tm_mday);
			scanf_s(" %d%*[-/: ] ", &calendar.tm_hour);
			scanf_s(" %d", &calendar.tm_min);
			start = mktime(&calendar);
			if (start < 0)
			{
				printf("Wrong input\n");
				break;
			}

			/* 할일의 실행기간을 아래의 패턴으로 입력받아 time_t로 저장*/
			printf("period(DDDD hh:mm):");
			scanf_s(" %d%*[-/: ] ", &calendar.tm_mday);
			scanf_s(" %d%*[-/: ] ", &calendar.tm_hour);
			scanf_s(" %d", &calendar.tm_min);
			getchar(); /* stdin 개행 문자 제거 */
			period = calendar.tm_mday * 3600 * 24
					+ calendar.tm_hour * 3600 + calendar.tm_min * 60;

			/* 할일의 자세한 내용을 문자열로 저장 */
			printf("detail:\n");
			gets_s(buf, ARRLEN(buf));

			todo_addTodo(&todoMan, start, period, buf);

			break;

		case '2':
			/* 할일 지우기 */
			printf("deleting todo...\n");
			/* 지울 할일들의 처음 시작 시간을 아래의 패턴으로 입력받아 time_t로 저장*/
			printf("from start( pattern like 'YYYY/MM/DD hh:mm' OR -1(all) ):\n");
			scanf_s(" %d", &calendar.tm_year);
			if (calendar.tm_year == -1)
			{
				start = 0;
				end = 1;
				end = ~( end << ( BITSIZEOF(end) - 1 ) ); /* Signed max value of time_t */
			}
			else
			{
				calendar.tm_year -= 1900;
				scanf_s("%*[-/: ] %d%*[-/: ] ", &calendar.tm_mon);
				--calendar.tm_mon;
				scanf_s(" %d%*[-/: ] ", &calendar.tm_mday);
				scanf_s(" %d%*[-/: ] ", &calendar.tm_hour);
				scanf_s(" %d", &calendar.tm_min);
				start = mktime(&calendar);
				if (start < 0)
				{
					printf("Wrong input\n");
					break;
				}

				/* 지울 할일들의 마지막 시작 시간을 아래의 패턴으로 입력받아 time_t로 저장*/
				printf("to start(YYYY/MM/DD hh:mm):");
				scanf_s(" %d%*[-/: ] ", &calendar.tm_year);
				calendar.tm_year -= 1900;
				scanf_s(" %d%*[-/: ] ", &calendar.tm_mon);
				--calendar.tm_mon;
				scanf_s(" %d%*[-/: ] ", &calendar.tm_mday);
				scanf_s(" %d%*[-/: ] ", &calendar.tm_hour);
				scanf_s(" %d", &calendar.tm_min);
				end = mktime(&calendar);
				if (end < 0)
				{
					printf("Wrong input\n");
					break;
				}
			}

			printf("%zu todos deleted\n", todo_delTodo(&todoMan, start, end));

			break;

		case '3':
			/* 할일 찾기 */
			printf("querying todo...\n");
			/* 찾을 할일들의 처음 시작 시간을 아래의 패턴으로 입력받아 time_t로 저장*/
			printf("from start( pattern like 'YYYY/MM/DD hh:mm' OR -1(all) ):\n");
			scanf_s(" %d", &calendar.tm_year);
			if (calendar.tm_year == -1)
			{
				start = 0;
				end = 1;
				end = ~( end << ( BITSIZEOF(end) - 1 ) ); /* Signed max value of time_t */
			}
			else
			{
				calendar.tm_year -= 1900;
				scanf_s("%*[-/: ] %d%*[-/: ] ", &calendar.tm_mon);
				--calendar.tm_mon;
				scanf_s(" %d%*[-/: ] ", &calendar.tm_mday);
				scanf_s(" %d%*[-/: ] ", &calendar.tm_hour);
				scanf_s(" %d", &calendar.tm_min);
				start = mktime(&calendar);
				if (start < 0)
				{
					printf("Wrong input\n");
					break;
				}

				/* 찾을 할일들의 마지막 시작 시간을 아래의 패턴으로 입력받아 time_t로 저장*/
				printf("to start(YYYY/MM/DD hh:mm):");
				scanf_s(" %d%*[-/: ] ", &calendar.tm_year);
				calendar.tm_year -= 1900;
				scanf_s(" %d%*[-/: ] ", &calendar.tm_mon);
				--calendar.tm_mon;
				scanf_s(" %d%*[-/: ] ", &calendar.tm_mday);
				scanf_s(" %d%*[-/: ] ", &calendar.tm_hour);
				scanf_s(" %d", &calendar.tm_min);
				end = mktime(&calendar);
				if (end < 0)
				{
					printf("Wrong input\n");
					break;
				}
			}

			/* todoIter를 시작 시간부터 종료 시간까지 진행시킨다 */
			for (int i5 = todo_fromTodo(&todoMan, &todoIter, start);
					i5;
					i5 = todo_untilTodo(&todoIter, end))
			{
				struct todo_Todo * td = todo_getTodo(&todoIter);
				printf("detail: %s\n", td->detail);
				ctime_s(buf, ARRLEN(buf), &td->start);
				printf("start : %s", buf);
				const time_t temp = td->start + td->period;
				ctime_s(buf, ARRLEN(buf), &temp);
				printf("end   : %s", buf);
				puts("");
			}
			todo_iterUnref(&todoIter);

			break;

		case '4':
			/* 프로그램 종료 */
			/* 모든 동적할당 해제 */
			start = 0;
			end = 1;
			end = ~( end << ( BITSIZEOF(end) - 1 ) ); /* Signed max value of time_t */
			todo_delTodo(&todoMan, start, end);

			goto gotoExitMain;
		}

		puts("");
	}

gotoExitMain:;

	return 0;
}
