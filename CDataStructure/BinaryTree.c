/**
* Binary Tree
*
* Public 함수에 대한 문서는 BinaryTree.h에 있음
*/


#include <stddef.h>

#include "../customdef.h"

#include "BinaryTree.h"


void BinaryTree_postfix_initNode(BinaryTreeNode_postfix* node)
{
	node->parent = NULL;
	node->left = NULL;
	node->right = NULL;
}


BinaryTreeNode_postfix* BinaryTree_postfix_makeNode(void)
{
	BinaryTreeNode_postfix* node;
	if (!ASSIGN_TOTAL(malloc, node, 1))
	{
		return NULL;
	}

	BinaryTree_postfix_initNode(node);

	return node;
}


void BinaryTree_postfix_destroyNode(BinaryTreeNode_postfix* node)
{
	free(node);
}


BinaryTreeData BinaryTree_postfix_getData(const BinaryTreeNode_postfix* node)
{
	return node->data;
}


BinaryTreeData* BinaryTree_postfix_getDataPtr(
	BinaryTreeNode_postfix* node)
{
	return &node->data;
}


void BinaryTree_postfix_setData(
	BinaryTreeNode_postfix* node, BinaryTreeData data)
{
	node->data = data;
}


BinaryTreeNode_postfix* BinaryTree_postfix_getLeft(
	const BinaryTreeNode_postfix* node)
{
	return node->left;
}


BinaryTreeNode_postfix* BinaryTree_postfix_getRight(
	const BinaryTreeNode_postfix* node)
{
	return node->right;
}


BinaryTreeNode_postfix* BinaryTree_postfix_getParent(
	const BinaryTreeNode_postfix* node)
{
	return node->parent;
}


static BinaryTreeNode_postfix* BinaryTree_postfix_getLRmost(
	BinaryTreeNode_postfix* node,
	BinaryTreeNode_postfix*(* binaryTree_getLR)(const BinaryTreeNode_postfix*))
{
	if (!node)
	{
		return NULL;
	}

	BinaryTreeNode_postfix* result;
	result = node;
	for (
		BinaryTreeNode_postfix* next;
		next = binaryTree_getLR(result);
		result = next) ;

	return result;
}


BinaryTreeNode_postfix* BinaryTree_postfix_getLeftmost(
	BinaryTreeNode_postfix* node)
{
	return BinaryTree_postfix_getLRmost(node, BinaryTree_postfix_getLeft);
}


BinaryTreeNode_postfix* BinaryTree_postfix_getRightmost(
	BinaryTreeNode_postfix* node)
{
	return BinaryTree_postfix_getLRmost(node, BinaryTree_postfix_getRight);
}


static BinaryTreeNode_postfix* BinaryTree_postfix_change(
	BinaryTreeNode_postfix* parent,
	BinaryTreeNode_postfix* child,
	BinaryTreeNode_postfix** substituted)
{
	if (child)
	{
		if (child->parent)
		{
			if (child->parent->left == child)
			{
				child->parent->left = NULL;
			}
			if (child->parent->right == child)
			{
				child->parent->right = NULL;
			}
		}
		child->parent = parent;
	}
	
	BinaryTreeNode_postfix* result;
	/* child를 루트로 만들려고 하지 않는 경우*/
	if (parent)
	{
		result = *substituted;
		if (result)
		{
			result->parent = NULL;
		}
		*substituted = child;
	}
	/* child를 루트로 만들려고 하는 경우*/
	else
	{
		result = NULL;
	}

	return result;
}


BinaryTreeNode_postfix* BinaryTree_postfix_changeLeft(
	BinaryTreeNode_postfix* parent,
	BinaryTreeNode_postfix* child)
{
	return BinaryTree_postfix_change(parent, child, &parent->left);
}


BinaryTreeNode_postfix* BinaryTree_postfix_changeRight(
	BinaryTreeNode_postfix* parent,
	BinaryTreeNode_postfix* child)
{
	return BinaryTree_postfix_change(parent, child, &parent->right);
}


void BinaryTree_postfix_swap(
	BinaryTreeNode_postfix* a, BinaryTreeNode_postfix* b)
{
	BinaryTreeNode_postfix* aTemp, * bTemp;

	aTemp = a->parent;
	bTemp = b->parent;
	if (aTemp)
	{
		if (aTemp->left == a)
		{
			aTemp->left = b;
		}
		if (aTemp->right == a)
		{
			aTemp->right = b;
		}
	}
	if (bTemp)
	{
		if (bTemp->left == b)
		{
			bTemp->left = a;
		}
		if (bTemp->right == b)
		{
			bTemp->right = a;
		}
	}
	a->parent = bTemp;
	b->parent = aTemp;

	aTemp = BinaryTree_postfix_getLeft(a);
	bTemp = BinaryTree_postfix_getLeft(b);
	BinaryTree_postfix_changeLeft(a, bTemp);
	BinaryTree_postfix_changeLeft(b, aTemp);

	aTemp = BinaryTree_postfix_getRight(a);
	bTemp = BinaryTree_postfix_getRight(b);
	BinaryTree_postfix_changeRight(a, bTemp);
	BinaryTree_postfix_changeRight(b, aTemp);
}


int BinaryTree_postfix_isRoot(const BinaryTreeNode_postfix* node)
{
	return !node->parent;
}


int BinaryTree_postfix_isLeaf(const BinaryTreeNode_postfix* node)
{
	return !( node->left || node->right );
}


size_t BinaryTree_postfix_travPre(
	const BinaryTreeNode_postfix* node, void(* action)(BinaryTreeData))
{
	if (!node)
	{
		return 0;
	}

	action(node->data);

	return 1 +
		BinaryTree_postfix_travPre(node->left, action) +
		BinaryTree_postfix_travPre(node->right, action);
}


size_t BinaryTree_postfix_travIn(
	const BinaryTreeNode_postfix* node, void(* action)(BinaryTreeData))
{
	if (!node)
	{
		return 0;
	}

	size_t result = 1;
	result += BinaryTree_postfix_travIn(node->left, action);
	action(node->data);
	result += BinaryTree_postfix_travIn(node->right, action);

	return result;
}


size_t BinaryTree_postfix_travPost(
	const BinaryTreeNode_postfix* node, void(* action)(BinaryTreeData))
{
	if (!node)
	{
		return 0;
	}

	size_t result = 1;
	result += BinaryTree_postfix_travPost(node->left, action);
	result += BinaryTree_postfix_travPost(node->right, action);
	action(node->data);

	return result;
}


size_t BinaryTree_postfix_travLeaf(
	const BinaryTreeNode_postfix* node, void(* action)(BinaryTreeData))
{
	if (!node)
	{
		return 0;
	}

	if (BinaryTree_postfix_isLeaf(node))
	{
		action(node->data);

		return 1;
	}

	return
		BinaryTree_postfix_travLeaf(
			BinaryTree_postfix_getLeft(node), action)
		+ BinaryTree_postfix_travLeaf(
			BinaryTree_postfix_getRight(node), action);
}
