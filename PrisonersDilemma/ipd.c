#define _CRT_SECURE_NO_WARNINGS

#include "ipd.h"

//함수(전략) 함수포인터 지정 
int IPD_indicate(void) {
	player[0].tactic = titfortat;
	player[1].tactic = PotatOS;
	player[2].tactic = downing;
	player[3].tactic = random;
	player[4].tactic = thekind;
	player[5].tactic = Gahz_rilla;
	player[6].tactic = Fool;
	player[7].tactic = compensationDemander;
	player[8].tactic = criminal;
	return 0;
}

//시행 과정 
int IPD_match(int p1, int p2) {
	int i;
	int p1_ch, p2_ch, p1_tmp, p2_tmp;
	int p1_acq = 0, p1_acc = 0;
	int p2_acq = 0, p2_acc = 0;
	char p1_adr[64], p2_adr[64];
	FILE *p1_fp, *p2_fp;
	
	//로그를 기록할 텍스트파일 생성(기록) 
	sprintf(p1_adr, "Log\\Match %d[%d]\\Player %d\\vs Player %d.txt", rpt, trial, p1, p2);
	sprintf(p2_adr, "Log\\Match %d[%d]\\Player %d\\vs Player %d.txt", rpt, trial, p2, p1);
	p1_fp = fopen(p1_adr, "w"), p2_fp = fopen(p2_adr, "w");
	
	for(i=0; i<trial; i++) {
		if(i == 0) {
			//첫번째 시행에서는 42를 줌 
			p1_tmp = player[p1].tactic(42);
			p2_tmp = player[p2].tactic(42);				
		} else {
			//나머지 시행에서는 상대의 이전결정을 줌 				
			p1_tmp = player[p1].tactic(p2_ch);
			p2_tmp = player[p2].tactic(p1_ch);			
		}
		
		//0 또는 1이외의 반환값은 0으로 처리 
		if(p1_tmp != 0 && p1_tmp != 1) p1_tmp = 0;
		if(p2_tmp != 0 && p2_tmp != 1) p2_tmp = 0;
		
		//예외가 없다면 결정 확정 
		p1_ch = p1_tmp, p2_ch = p2_tmp;
		
		//점수배분
		if (!p1_ch && !p2_ch) {
			player[p1].score += 3, player[p2].score += 3;
			p1_acq = 3, p2_acq = 3;
			p1_acc +=3, p2_acc +=3;
		}              
		if (p1_ch && !p2_ch) {
			player[p1].score += 5;
			p1_acq = 5, p2_acq = 0;
			p1_acc +=5, p2_acc +=0;
		}
		if (!p1_ch && p2_ch) {
			player[p2].score += 5;
			p1_acq = 0, p2_acq = 5;
			p1_acc +=0, p2_acc +=5;
		}
		if (p1_ch && p2_ch) {
			player[p1].score++, player[p2].score++;
			p1_acq = 1, p2_acq = 1;
			p1_acc +=1, p2_acc +=1;
		} 
		
		fprintf(p1_fp, "%d || %d\t현재:%d\t누적:%d\n", p1_ch, p2_ch, p1_acq, p1_acc);
		fprintf(p2_fp, "%d || %d\t현재:%d\t누적:%d\n", p2_ch, p1_ch, p2_acq, p2_acc);
	}
	
	//매치가 종료했음을 알 수 있게 24 
	player[p1].tactic(24);
	player[p2].tactic(24);
	
	fclose(p1_fp);
	fclose(p2_fp); 
	
	return 0;
}

//로그삭제 
int IPD_directory(int x) {
	int i, j, k;
	char adress[64];
	FILE *fpRmvR, *fpRmvW;	//FilePointerRemoveRead, FilePointerRemoveWrite 
	
	switch(x) {
		case 0: {
			if(_access("Log\\", 0) == 0) {
			printf("이전 로그기록이 존재하므로 삭제후 진행합니다.\n"); 
			fpRmvR = fopen("Remove.txt", "r");
			if(fpRmvR == NULL) {
				printf("Error : Remove.txt와 Log 디렉토리를 삭제후 다시 진행해주세요\n"); 
				exit(-1);
			}
			while(!feof(fpRmvR)) {
				fgets(adress, 64, fpRmvR);	
				adress[strlen(adress)-1] = '\0'; 
				if(_rmdir(adress) != 0) {
					if(_unlink(adress) != 0) {
						printf("%s", adress);
						printf("파일을 삭제하지 못했습니다\n");
					}
				}
			}
			fclose(fpRmvR);
			}
			break;
		}
		
		case 1: {
			fpRmvW = fopen("Remove.txt", "w");
			for(i=0; i<REPEAT; i++) {
				for(j=0; j<PLAYERS; j++) {
					for(k=0; k<PLAYERS; k++) {
						if(k != j) 
							fprintf(fpRmvW, "Log\\\\Match %d[%d]\\\\Player %d\\\\vs Player %d.txt\n", i, trnum[i], j, k);
					}
				}
			}
			for(i=0; i<REPEAT; i++) {
				for(j=0; j<PLAYERS; j++) {
					fprintf(fpRmvW, "Log\\\\Match %d[%d]\\\\Player %d\\\\\n", i, trnum[i], j);
				}
			}
			for(i=0; i<REPEAT; i++) {
				fprintf(fpRmvW, "Log\\\\Match %d[%d]\\\\\n", i, trnum[i]);
			}
			fprintf(fpRmvW, "Log\\\\Report.txt\n");
			fprintf(fpRmvW, "Log\\\\");
			fclose(fpRmvW); 
			break;
		}
	}
	
	return 0;
}
