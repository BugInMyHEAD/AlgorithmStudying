int compensationDemander(int op)
{
	enum OP { SILENCE=0, BETRAYAL=1, END=24, START=42, };

	const int damageInitVal = -3;

	static int damage;

	enum OP retVal;

	switch (op)
	{
	case SILENCE:
		if (damage > 0)
		{
			--damage;
			retVal = BETRAYAL;
		}
		else
		{
			retVal = SILENCE;
		}
		break;

	case BETRAYAL:
		if (-1 == damage++)
		{
			retVal = SILENCE;
		}
		else
		{
			retVal = BETRAYAL;
		}
		break;

	case END:
		damage = damageInitVal;
		retVal = BETRAYAL;
		break;

	case START:
		damage = damageInitVal;
		retVal = SILENCE;
		break;
	}

	return retVal;
}