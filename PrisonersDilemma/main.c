#define _CRT_SECURE_NO_WARNINGS

#include "ipd.h"

int main(void) {
	int i = 0, j;
	char adress[64];	//폴더 생성, 삭제를 위한 문자열 저장 
	FILE *fp_report;
	time_t cttime;
	
	memset(adress, 0, sizeof(adress));
	srand((unsigned)time(NULL));
	
	//참가 전략 
	IPD_indicate();
	
	//로그 존재시 삭제후 진행 
	if(IPD_directory(0) != 0) {
		printf("디렉터리를 삭제하지 못했습니다.\n");
	}
	
	printf("계속 진행합니까?\n");
	getchar(); 
	
	//로그 폴더 생성
	_mkdir("Log\\");
	
	for(rpt=0; rpt<REPEAT; rpt++) {
		//시행 횟수 결정(최소 1) 
		trial = 1;
		while(rand()%100 <= 95) {
			trial++;
		}
		trnum[rpt] = trial;
		//매치당 폴더 생성 
		sprintf(adress, "Log\\Match %d[%d]", rpt, trial);
		_mkdir(adress);
		
		//각 플레이어들의 폴더 생성 
		for(i=0; i<PLAYERS; i++) {
			sprintf(adress, "Log\\Match %d[%d]\\Player %d\\", rpt, trial, i);
			_mkdir(adress);
		}
		
		//매치	
		for(i=0; i<PLAYERS-1; i++) {
			for(j=i+1; j<PLAYERS; j++) {
				if(IPD_match(i, j) != 0)
					printf("ERROR : Match %d 에서 Player %d, Player %d의 시행이 정상적으로 이루어지지 않았습니다.", rpt, i, j); 
			}
		}
		printf("%d번째 매치가 진행중에 있습니다...\n", rpt);
	}
	
	//보고서 작성후 마무리
	time(&cttime);
	fp_report = fopen("Log\\Report.txt", "w"); 
	fprintf(fp_report, "로그를 기록한 시각 : "); 
	fprintf(fp_report, ctime(&cttime));
	for(i=0; i<PLAYERS; i++) {
		fprintf(fp_report, "Player %d : %d\n", i, player[i].score);
	}
	fclose(fp_report);
	
	//로그 생성 기록 
	if(IPD_directory(1) != 0) {
		printf("디렉터리 생성 로그를 작성하지 못했습니다.\n");
	}

	printf("시뮬레이션이 마무리 되었습니다.\n");
	
	return 0;
}
