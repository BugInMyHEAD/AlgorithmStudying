#배점
둘다 침묵 : 둘다 모두 3점씩 획득
한명 배신,한명 침묵 : 배신은5점, 침묵은 0점획득
둘다 배신 : 둘다 모두 1점씩 획득

#플레이어 전략
player[0].tactic = random;
player[1].tactic = titfortat;
player[2].tactic = downing;
player[3].tactic = AA;
player[4].tactic = thekind;
player[5].tactic = criminal;
등으로 설정합니다.

#사용방법
1. tactic.h에 자신의 함수를 선언합니다.
2-1. .c파일을 생성하고 자신의 함수를 정의합니다.
2-2. 또는 base_tactic.c 파일에 자신의 함수를 정의합니다.
3. ipd.h 에서 플레이어 수, 매치 반복 횟수를 정합니다.
4. 참가할 플레이어를 ipd.c의 IPD_indicate에 작성합니다.
5. 시뮬레이션을 할 준비가 완료되었다면 시뮬레이터를 실행합니다.
6. 매치결과와 점수는 실행파일의 .\log 에서 확인할 수 있습니다.

#주의사항
Remove.txt을 수정하지 마세요. 프로그램 오작동의 원인이 될 수 있습니다.

#요구사항
없음.

마지막 기록 날짜
오후 8:47 2018-01-15