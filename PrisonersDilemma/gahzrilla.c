#include "tactic.h"

int Gahz_rilla(int op) {
	int Attack = 6, Life = 9;
	int rtvalue = 0;
	
	if(op == 1) {
		Attack *= 2;
		Life -= 1;
	} else if(Life > 0) {
		Life += 1;
	}
	
	if(Attack <= 6) {
		rtvalue = 0;
	}
	else {
		Attack -= 2;
		rtvalue = 1;
	}
	
	if(Life <= 0) {
		rtvalue = 0;
	}
	
	return rtvalue;
}
