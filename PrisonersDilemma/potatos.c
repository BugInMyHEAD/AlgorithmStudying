#include "tactic.h"

int PotatOS(int op) {
   static int status = 0, cnt = 0; //status 0:test, 1:attack, 2:TFT, 3:wake, 4~5:sleep
   
	if(op == 24) {
		status = 0, cnt = 0;
		return -1;
	}
	cnt++;
	if(op == 42) return 1;
   
   switch (status) {
		case 0: {
			if(cnt == 2 && op == 0) {
				status = 5;
				return 1;
			}
			if(cnt == 3 && op == 0) {
				status = 1;
				return 1;
			}
			if(cnt == 3 && op == 1) {
				status = 5;
				return 0;
			}
			break;
		}
		case 1: 
			return 1;
		case 2: 
			return op;
		case 3: {
			if(op == 1) {
				status = 1;
				return 1;
			} else {
				status = 2;
				return 0;
			}
		}
		case 4: {
			status--; 
			return 0;
		}
		case 5: {
			status--; 
			return 0;
		}
   }
   
   return 0;
}
