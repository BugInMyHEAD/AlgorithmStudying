#ifndef IPD_H
#define IPD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <io.h>
#include <direct.h>
#include "tactic.h"

#define PLAYERS 9			//플레이어 수(수정가능) 
#define REPEAT 	100			//매치 반복 횟수(수정가능) 
#define w 		95			//할인 계수

int trial;					//w에 의한 시행 수 
int rpt;					//현재 매치 반복 횟수
int trnum[REPEAT];			//각 매치당 시행 수						

struct players {			//각 플레이어 들의 전략, 점수 구조체 
	int score;
	int (*tactic)(int);
} player[PLAYERS];

int IPD_match(int, int);	//실체 시행 
int IPD_directory(int);		//로그 삭제 
int IPD_indicate(void); 	//전략 지정
int IPD_trial(int); 

#endif
