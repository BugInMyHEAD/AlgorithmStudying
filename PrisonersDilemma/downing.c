#include "tactic.h"

int downing(int op) {
	static int gamecnt = 0;
	static int opchs[200] = {-1,};
	int i, scnt=0, tcnt=0;
	
	if(op == 42) {
		for(i=0; i<200; i++) {
			opchs[i] = -1;
		}
		return 1;
	}
	if(op == 24) {
		for(i=0; i<200; i++) {
			opchs[i] = -1;
		}
		gamecnt = 0, scnt = 0, tcnt = 0;
		return -1;
	}
	
	opchs[gamecnt++] = op;
	
	for(i=0; i<200; i++) {
		if(opchs[i] == 0) scnt++;
		if(opchs[i] == 1) tcnt++;
	}
	
	if(scnt > tcnt) return 0;
	else return 1;
}
