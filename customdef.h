#ifndef CUSTOMDEF_H_INCLUDED
#define CUSTOMDEF_H_INCLUDED


#include <limits.h>
#include <malloc.h>
#include <memory.h>
#include <stddef.h>


#define WHOSE(s, m, mptr) ( (s*)( (char*)mptr - offsetof(s, m) ) )
#define BITSIZEOF(x) ( sizeof(x) * CHAR_BIT )

#define ARRLEN(arr) ( sizeof(arr) / sizeof(*(arr)) )
#define ARRREV(arr, idx) ( arr[ARRLEN(arr) - 1 - idx] )
#define INCMOD(idx, len) ( ( (idx) + 1 ) % (len) )
#define DECMOD(idx, len) ( ( (idx) + (len) - 1 ) % (len) )
#define INCMOD_ASSIGN(idx, len) ( (idx) = INCMOD((idx), (len)) )
#define DECMOD_ASSIGN(idx, len) ( (idx) = DECMOD((idx), (len)) )


#ifdef __cplusplus
	#define FREE_ASSIGN(ptr) ( free(ptr), (ptr) = nullptr )
#else
	#define FREE_ASSIGN(ptr) ( free(ptr), (ptr) = NULL )
#endif

#define MALLOC_ASSIGN(ptr, len) \
	( (*(void**)&(ptr)) = malloc(sizeof(*(ptr)) * (len)) )

#define REALLOC_ASSIGN(dst, src, len) \
	( (*(void**)&(ptr)) = realloc((src), sizeof(*(dst)) * (len)) )

#define ASSIGN_TOTAL(func, ptr, len, ...) \
	( (*(void**)&(ptr)) = (func)(sizeof(*(ptr)) * (len)), __VA_ARGS__)

#define ASSIGN_PL(func, ptr, len, ...) \
	( (*(void**)&(ptr)) = (func)(sizeof(*(ptr)), (len), __VA_ARGS__))

#define ASSIGN_LP(func, len, ptr, ...) \
	( (*(void**)&(ptr)) = (func)((len), sizeof(*(ptr)), __VA_ARGS__))


/**
 * DO NOT input _s0w0a0p0_c0_v0a0r0_t0e0m0p0_ as an argument
 */
#define SWAP(a, b) \
do\
{\
	/* Zero division error makes possible to check the size equivalence */\
	char _s0w0a0p0_c0_v0a0r0_t0e0m0p0_[sizeof(a) / ( sizeof(a) == sizeof(b) )];\
	memcpy(_s0w0a0p0_c0_v0a0r0_t0e0m0p0_, &(a), sizeof(a));\
	(a) = (b);\
	memcpy(&(b), _s0w0a0p0_c0_v0a0r0_t0e0m0p0_, sizeof(b));\
} while (0)

 #define SWAP_EXPR(a, b, temp) \
 ( (temp) = (a), (a) = (b), (b) = (temp) )


#endif
